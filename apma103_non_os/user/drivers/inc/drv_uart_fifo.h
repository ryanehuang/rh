/*******************************************************************************
 *                            APPLICATION CODE
 *
 *(C) Copyright 2023; Co-Win Hydrogen Power (Guangdong) Co., Ltd
 *
 * @file    drv_uart_fifo.h
 * @brief   串口底层驱动
 * @author  XieXunjie
 * @version 0.0.2
 * @date    2023-03-02
 *
 * -----------------------------------------------------------------------------
 *
 * -----------------------------------------------------------------------------
 * 文件修改历史：
 * <时间>           | <版本>    | <作者>        | <描述>
 * <2023-03-02>     | v0.0.2    | XieXunjie     | 串口底层驱动
 * <2023-01-13>     | v0.0.1    | XieXunjie     | 模板文件
 * -----------------------------------------------------------------------------
 ******************************************************************************/

#ifndef __DRV_UART_FIFO_H
#define __DRV_UART_FIFO_H

/*
********************************************************************************
*                                包含头文件
********************************************************************************
*/
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "drv_data_type.h"

/*
********************************************************************************
*                              	定义外部宏指令
********************************************************************************
*/

/*
********************************************************************************
*                             	 外部数据类型
********************************************************************************
*/
/* 定义端口号 */
//typedef enum
//{
//	COM1 = 0,	/* USART1  PA9, PA10 */
//	COM2 = 1,	/* USART2, PA2, PA3 */
//	COM3 = 2,	/* USART3, PB10, PB11 */
//	COM4 = 3,	/* UART4, PC10, PC11 */
//	COM5 = 4,	/* UART5, PC12, PD2 */
//}COM_PORT_E;

typedef enum
{
    COM1 = 0,
    COM2 = 1,
	 COM3 = 2,
} COM_TypeDef;

///* 串口设备结构体 */
//typedef struct
//{
//	USART_TypeDef *uart;		/* STM32内部串口设备指针 */
//	uint8_t *pTxBuf;			/* 发送缓冲区 */
//	uint8_t *pRxBuf;			/* 接收缓冲区 */
//	uint16_t usTxBufSize;		/* 发送缓冲区大小 */
//	uint16_t usRxBufSize;		/* 接收缓冲区大小 */
//	__IO uint16_t usTxWrite;			/* 发送缓冲区写指针 */
//	__IO uint16_t usTxRead;			/* 发送缓冲区读指针 */
//	__IO uint16_t usTxCount;			/* 等待发送的数据个数 */

//	__IO uint16_t usRxWrite;			/* 接收缓冲区写指针 */
//	__IO uint16_t usRxRead;			/* 接收缓冲区读指针 */
//	__IO uint16_t usRxCount;			/* 还未读取的新数据个数 */

//	void (*SendBefor)(void); 	/* 开始发送之前的回调函数指针（主要用于RS485切换到发送模式） */
//	void (*SendOver)(void); 	/* 发送完毕的回调函数指针（主要用于RS485将发送模式切换为接收模式） */
//	void (*ReciveNew)(uint8_t _byte);	/* 串口收到数据的回调函数指针 */
//}UART_T;

/*
********************************************************************************
*                               外部调用常量
********************************************************************************
*/


/*
********************************************************************************
*                              	供外部调用的函数声明
********************************************************************************
*/



/*初始化串口硬件*/
void Drv_InitUart(void);
//void comSendBuf(COM_PORT_E _ucPort, uint8_t *_ucaBuf, uint16_t _usLen);
//void comSendChar(COM_PORT_E _ucPort, uint8_t _ucByte);
//uint8_t comGetChar(COM_PORT_E _ucPort, uint8_t *_pByte);

//void comClearTxFifo(COM_PORT_E _ucPort);
//void comClearRxFifo(COM_PORT_E _ucPort);

//数据串口接收字节回调函数
void Set_ReciveNew_COM1_Callback(CPU_FNCT_UINT8_T p);
void Set_ReciveNew_COM2_Callback(CPU_FNCT_UINT8_T p);
void Set_ReciveNew_COM3_Callback(CPU_FNCT_UINT8_T p);
/*底层串口测试*/
void Drv_Uart_TEST(void);
#endif  //__DRV_UART_FIFO_H

/***************************** (文件结束) *********************************/
