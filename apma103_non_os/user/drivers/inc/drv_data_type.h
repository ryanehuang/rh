/*******************************************************************************
 *                            APPLICATION CODE
 *
 *           (c) Copyright 2023; Co-Win Hydrogen Power (Guangdong) Co., Ltd
 *
 * @file    drv_data_type.h
 * @brief   数据结构体模块
 * @author  XieXunjie
 * @version 0.0.3
 * @date    2023-01-13
 *
 * -----------------------------------------------------------------------------
 *
 * -----------------------------------------------------------------------------
 * 文件修改历史：
 * <时间>       | <版本>    | <作者>    | <描述>
 * 2023-01-13   | v0.0.3   | XieXunjie | 重构文件
 * -----------------------------------------------------------------------------
 ******************************************************************************/


#ifndef __BSP_DATA_TYPE_H
#define __BSP_DATA_TYPE_H

/*
********************************************************************************
*                                包含头文件
********************************************************************************
*/
#include <stdint.h>


/*
********************************************************************************
*                              	定义外部宏指令
********************************************************************************
*/

#ifndef TRUE
	#define TRUE  1
#endif

#ifndef FALSE
	#define FALSE 0
#endif

#define  DEF_FALSE                                         0u
#define  DEF_TRUE                                          1u

#define  DEF_NO                                            0u
#define  DEF_YES                                           1u

#define  DEF_DISABLED                                      0u
#define  DEF_ENABLED                                       1u

#define  DEF_INACTIVE                                      0u
#define  DEF_ACTIVE                                        1u

#define  DEF_INVALID                                       0u
#define  DEF_VALID                                         1u

#define  DEF_OFF                                           0u
#define  DEF_ON                                            1u

#define  DEF_CLR                                           0u
#define  DEF_SET                                           1u

#define  DEF_FAIL                                          0u
#define  DEF_OK                                            1u



#ifndef  TRACE_LEVEL_OFF
#define  TRACE_LEVEL_OFF                0
#endif

#ifndef  TRACE_LEVEL_INFO
#define  TRACE_LEVEL_INFO               1
#endif

#ifndef  TRACE_LEVEL_DBG
#define  TRACE_LEVEL_DBG                2
#endif

#define  APP_CFG_TRACE_LEVEL             TRACE_LEVEL_OFF//TRACE_LEVEL_INFO//TRACE_LEVEL_OFF
#define  APP_CFG_TRACE                   printf

#define  BSP_CFG_TRACE_LEVEL             TRACE_LEVEL_OFF
#define  BSP_CFG_TRACE                   printf

#define  APP_TRACE_INFO(x)               ((APP_CFG_TRACE_LEVEL >= TRACE_LEVEL_INFO)  ? (void)(APP_CFG_TRACE x) : (void)0)
#define  APP_TRACE_DBG(x)                ((APP_CFG_TRACE_LEVEL >= TRACE_LEVEL_DBG)   ? (void)(APP_CFG_TRACE x) : (void)0)

#define  BSP_TRACE_INFO(x)               ((BSP_CFG_TRACE_LEVEL  >= TRACE_LEVEL_INFO) ? (void)(BSP_CFG_TRACE x) : (void)0)
#define  BSP_TRACE_DBG(x)                ((BSP_CFG_TRACE_LEVEL  >= TRACE_LEVEL_DBG)  ? (void)(BSP_CFG_TRACE x) : (void)0)



/*
********************************************************************************
*                             	 外部数据类型
********************************************************************************
*/

typedef            void        CPU_VOID;
typedef            char        CPU_CHAR;                        /*  8-bit character                                     */
typedef  unsigned  char        CPU_BOOLEAN;                     /*  8-bit boolean or logical                            */
typedef  unsigned  char        CPU_INT08U;                      /*  8-bit unsigned integer                              */
typedef    signed  char        CPU_INT08S;                      /*  8-bit   signed integer                              */
typedef  unsigned  short       CPU_INT16U;                      /* 16-bit unsigned integer                              */
typedef    signed  short       CPU_INT16S;                      /* 16-bit   signed integer                              */
typedef  unsigned  int         CPU_INT32U;                      /* 32-bit unsigned integer                              */
typedef    signed  int         CPU_INT32S;                      /* 32-bit   signed integer                              */
typedef  unsigned  long  long  CPU_INT64U;                      /* 64-bit unsigned integer                              */
typedef    signed  long  long  CPU_INT64S;                      /* 64-bit   signed integer                              */

typedef            float       CPU_FP32;                        /* 32-bit floating point                                */
typedef            double      CPU_FP64;                        /* 64-bit floating point                                */


typedef  volatile  CPU_INT08U  CPU_REG08;                       /*  8-bit register                                      */
typedef  volatile  CPU_INT16U  CPU_REG16;                       /* 16-bit register                                      */
typedef  volatile  CPU_INT32U  CPU_REG32;                       /* 32-bit register                                      */
typedef  volatile  CPU_INT64U  CPU_REG64;                       /* 64-bit register                                      */

//void xx(void)
typedef void      (*CPU_FNCT_VOID)			(void);            /* See Note #2a.                                        */

typedef uint8_t   (*UINT8_T_FNCT_VOID)		(void);            
//void xx(void *)
typedef void      (*CPU_FNCT_PTR )			(void *p_obj);     /* See Note #2b. */
//void xx(uint32_t ,uint32_t ,uint8_t *,uint8_t )
typedef void      (*CPU_FNCT_CAN)			(uint32_t ,uint32_t ,uint8_t *,uint8_t,uint8_t);        
typedef void      (*CPU_FNCT_CAN_xx)		(uint32_t ,uint32_t ,uint8_t *,uint16_t,uint8_t);

//void xx(uint8_t)
typedef void      (*CPU_FNCT_UINT8_T)		(uint8_t);

typedef void      (*CPU_FNCT_UINT16_T)		(uint16_t);

typedef void      (*CPU_FNCT_RT)			(uint8_t *,uint16_t );
typedef uint16_t  (*CPU_FNCT_RT_UINT16_T)	(uint8_t *,uint16_t );

typedef void      (*CPU_FNCT_xx)			(uint8_t,uint8_t *,uint8_t );

typedef void 	 (*StorageWrite)			(uint8_t* ,uint32_t ,uint16_t );
typedef void 	 (*StorageRead)				(uint8_t* ,uint32_t ,uint16_t );  
/*通知类型*/
typedef void 	 (*Notify)					(uint16_t , uint16_t , uint8_t );
typedef void 	 (*Notify_String)			(uint16_t , uint16_t , uint8_t* );


/*
********************************************************************************
*                               外部调用常量
********************************************************************************
*/


/*
********************************************************************************
*                              	供外部调用的函数声明
********************************************************************************
*/



#endif  //__BSP_DATA_TYPE_H

/***************************** (文件结束) *********************************/
