/*******************************************************************************
 *                            APPLICATION CODE
 *
 *           (c) Copyright 2023; Co-Win Hydrogen Power (Guangdong) Co., Ltd
 *
 * @file    drv_tim_pwm.c
 * @brief   定时器PWM
 * @author  XieXunjie
 * @version 0.0.3
 * @date    2023-01-13
 *
 * -----------------------------------------------------------------------------
 *
 * -----------------------------------------------------------------------------
 * 文件修改历史：
 * <时间>       | <版本>    | <作者>    | <描述>
 * 2023-01-13   | v0.0.3    | XieXunjie | 重构文件
 * -----------------------------------------------------------------------------
 ******************************************************************************/

/*
********************************************************************************
*                              头文件包含
********************************************************************************
*/
#include "drv_tim_pwm.h"
#include "user_config.h"

/*
********************************************************************************
*                              	本地宏指令定义
********************************************************************************
*/
/*
PWM模式:同一个定时器使用,只能生成同频
PWM输出比较模式:生成多路不同频率的PWM
*/

#define PWM_OUTOUT_MAX			4


/*
********************************************************************************
*                              	局部变量
********************************************************************************
*/


/*
********************************************************************************
*                               全局变量
********************************************************************************
*/

/*
********************************************************************************
*                             	函数声明
********************************************************************************
*/

/*
	可以输出到GPIO的TIM通道:

	PA0  TIM5_CH1
	PA0  TIM5_CH2  TIM2_CH2
	PA2  TIM5_CH3  TIM2_CH3
	PA3  TIM5_CH4  TIM2_CH4
	
	PA6  TIM3_CH1
	PA7  TIM3_CH2
	
	PB0  TIM3_CH3
	PB1  TIM3_CH4

	PE9   TIM1_CH1
	PE11  TIM1_CH2
	PE13  TIM1_CH3

	PE14  TIM1_CH4
	
	PD12	TIM4_CH1
	PD13	TIM4_CH2
	PD14	TIM4_CH3
	PD15	TIM4_CH4	
	
	PC6		TIM8_CH1
	PC7		TIM8_CH2
	PC8		TIM8_CH3
	PC9		TIM8_CH4	
	
	PA8		TIM1_CH1
	PA9		TIM1_CH2
	PA10	TIM1_CH3
	PA11	TIM1_CH4	

	PB3		TIM2_CH2
	PB4		TIM3_CH1
	PB5		TIM3_CH2
	
	PB6		TIM4_CH1
	PB7		TIM4_CH2	
	PB8		TIM4_CH3
	PB9		TIM4_CH4		

	APB1 定时器有 TIM2, TIM3 ,TIM4, TIM5, TIM6, TIM7        --- 36M
	APB2 定时器有 TIM1, TIM8                    ---- 72M
*/



/*
********************************************************************************
*                              	函数定义
********************************************************************************
*/

/*!
 * @brief 设置PWM输出模式
 */
//static void SetPWMMode(void)
//{
//}

/*!
 * @brief 初始化PWM引脚
 */
static void pwm_gpio_config(void)
{
	GPIO_Config_T GPIO_ConfigStruct;
		
	RCM_EnableAPB2PeriphClock(RCM_APB2_PERIPH_TMR1);
	RCM_EnableAPB2PeriphClock(RCM_APB2_PERIPH_AFIO);
	RCM_EnableAPB2PeriphClock(RCM_APB2_PERIPH_GPIOE);
	GPIO_ConfigPinRemap(GPIO_FULL_REMAP_TMR1);

	GPIO_ConfigStruct.pin = GPIO_PIN_14;
	GPIO_ConfigStruct.mode = GPIO_MODE_AF_PP;
	GPIO_ConfigStruct.speed = GPIO_SPEED_50MHz;
	GPIO_Config(GPIOE, &GPIO_ConfigStruct);

	GPIO_ConfigStruct.pin = GPIO_PIN_13;
	GPIO_Config(GPIOE, &GPIO_ConfigStruct);
	GPIO_ConfigStruct.pin = GPIO_PIN_11;
	GPIO_Config(GPIOE, &GPIO_ConfigStruct);
	GPIO_ConfigStruct.pin = GPIO_PIN_9;
	GPIO_Config(GPIOE, &GPIO_ConfigStruct);
	
//	int i=0;
//	if(PWM_OUTOUT_MAX==0){
//	}else{
//		for(i=0;i<PWM_OUTOUT_MAX;i++)
//		{
//			SetPWMMode(i);
//		}
//	}
}

/*
*********************************************************************************************************
*	函 数 名: bsp_GetRCCofTIM
*	功能说明: 根据TIM 得到RCC寄存器
*	形    参：无
*	返 回 值: TIM外设时钟名
*********************************************************************************************************
*/
uint32_t bsp_GetRCCofTIM(TMR_T* TIMx)
{
	uint32_t rcc = 0;

	/*
		APB1 定时器有 TIM2, TIM3 ,TIM4, TIM5, TIM6, TIM7, TIM12, TIM13, TIM14
		APB2 定时器有 TIM1, TIM8 ,TIM9, TIM10, TIM11
	*/
	if (TIMx == TMR1)
	{
		rcc = RCM_APB2_PERIPH_TMR1;
	}
	else if (TIMx == TMR8)
	{
		rcc = RCM_APB2_PERIPH_TMR8;
	}
	else if (TIMx == TMR9)
	{
		//rcc = RCM_APB2_PERIPH_TMR9;
	}
	else if (TIMx == TMR10)
	{
		//rcc = RCM_APB2_PERIPH_TMR10;
	}
	else if (TIMx == TMR11)
	{
		//rcc = RCM_APB2_PERIPH_TMR111;
	}
	/* 下面是 APB1时钟 */
	else if (TIMx == TMR2)
	{
		rcc = RCM_APB1_PERIPH_TMR2;
	}
	else if (TIMx == TMR3)
	{
		rcc = RCM_APB1_PERIPH_TMR3;
	}
	else if (TIMx == TMR4)
	{
		rcc = RCM_APB1_PERIPH_TMR4;
	}
	else if (TIMx == TMR15)
	{
		rcc = RCM_APB1_PERIPH_TMR5;
	}
	else if (TIMx == TMR6)
	{
		rcc = RCM_APB1_PERIPH_TMR6;
	}
	else if (TIMx == TMR7)
	{
		rcc = RCM_APB1_PERIPH_TMR7;
	}
	else if (TIMx == TMR12)
	{
		//rcc = RCM_APB1_PERIPH_TMR12;
	}
	else if (TIMx == TMR13)
	{
		//rcc = RCM_APB1_PERIPH_TMR13;
	}
	else if (TIMx == TMR14)
	{
		//rcc = RCM_APB1_PERIPH_TMR14;
	}

	return rcc;
}

/*!
 * @brief 配置时基结构体
 */
static void Time_PWMOutputConfig(TMR_T* TIMx, uint8_t _ucChannel,uint32_t _ulFreq,uint32_t _ulDutyCycle)
{
	/*重装载值*/
	uint16_t usPeriod;
	/*预分频系数*/
	uint16_t usPrescaler;
	/*时钟*/
	uint32_t uiTIMxCLK;
	
	if ((TIMx == TMR1) || (TIMx == TMR8) || (TIMx == TMR9) || (TIMx == TMR10) || (TIMx == TMR11))
	{
		/* APB2 定时器 */
		uiTIMxCLK = SystemCoreClock;
	}
	else	/* APB1 定时器 */
	{
		uiTIMxCLK = SystemCoreClock/2;	// SystemCoreClock / 2;
	}
	if (_ulFreq < 100)
	{
		usPrescaler = 10000 - 1;					/* 分频比 = 10000 */
		usPeriod =  (uiTIMxCLK / 10000) / _ulFreq  - 1;		/* 自动重装的值 */
	}
	else if (_ulFreq < 3000)
	{
		usPrescaler = 100 - 1;					/* 分频比 = 100 */
		usPeriod =  (uiTIMxCLK / 100) / _ulFreq  - 1;		/* 自动重装的值 */
	}
	else	/* 大于4K的频率，无需分频 */
	{
		usPrescaler = 0;					/* 分频比 = 1 */
		usPeriod = uiTIMxCLK / _ulFreq - 1;	/* 自动重装的值 */
	}
	
	TMR_BaseConfig_T TIM_TimeBaseStructure;        //配置时基结构体,声明一个结构体变量方便传参

	//=====================时基初始化======================//
	/* 使能TIM时钟 */
	if ((TIMx == TMR1) || (TIMx == TMR8))
	{
		RCM_EnableAPB2PeriphClock(bsp_GetRCCofTIM(TIMx));
	}
	else
	{
		RCM_EnableAPB1PeriphClock(bsp_GetRCCofTIM(TIMx));
	}
	/*预分频系数*/
	TIM_TimeBaseStructure.division = usPrescaler;
	/*重装载值*/
	TIM_TimeBaseStructure.period = usPeriod;
	// 计数器计数模式
	TIM_TimeBaseStructure.countMode = TMR_COUNTER_MODE_UP;
	// 时钟分频因子 - 一分频,配置死区时间需要用到
	TIM_TimeBaseStructure.clockDivision = TMR_CLOCK_DIV_1;
	// 重复寄存器的值,没有用到,不管
	TIM_TimeBaseStructure.repetitionCounter = 0;
	
	TMR_ConfigTimeBase(TIMx, &TIM_TimeBaseStructure);

	//=====================输出比较初始化======================//
	TMR_OCConfig_T TIM_OCInitStructure;                //配置输出比较结构体,声明一个结构体变量方便传参
	
	TMR_ConfigOCStructInit(&TIM_OCInitStructure);//初始化结构体成员
	TIM_OCInitStructure.mode = TMR_OC_MODE_PWM1;   //PWM模式1
	TIM_OCInitStructure.outputState = TMR_OC_STATE_ENABLE;       //TIM1通道2输出使能
	TIM_OCInitStructure.outputNState = TMR_OC_NSTATE_ENABLE;    //互补通道失能
	TIM_OCInitStructure.pulse = (_ulDutyCycle * usPeriod) / 10000;              //占空比
	TIM_OCInitStructure.polarity = TMR_OC_POLARITY_HIGH;           //低电平有效
	TIM_OCInitStructure.nPolarity = TMR_OC_NPOLARITY_HIGH;          //互补通道也是高电平有效
	TIM_OCInitStructure.idleState = TMR_OC_IDLE_STATE_RESET;        //空闲状态 低电平
	TIM_OCInitStructure.nIdleState = TMR_OC_NIDLE_STATE_RESET;      //互补通道空闲状态 低电平
	
	if (_ucChannel == 1)
	{
		TMR_ConfigOC1(TIMx, &TIM_OCInitStructure);
		TMR_ConfigOC1Preload(TIMx, TMR_OC_PRELOAD_ENABLE);
	}
	else if (_ucChannel == 2)
	{
		TMR_ConfigOC2(TIMx, &TIM_OCInitStructure);
		TMR_ConfigOC2Preload(TIMx, TMR_OC_PRELOAD_ENABLE);
	}
	else if (_ucChannel == 3)
	{
		TMR_ConfigOC3(TIMx, &TIM_OCInitStructure);
		TMR_ConfigOC3Preload(TIMx, TMR_OC_PRELOAD_ENABLE);
	}
	else if (_ucChannel == 4)
	{
		TMR_ConfigOC4(TIMx, &TIM_OCInitStructure);
		TMR_ConfigOC4Preload(TIMx, TMR_OC_PRELOAD_ENABLE);
	}
	 
	/* 设置自动装载模式*/
	TMR_EnableAutoReload(TIMx);
    TMR_Enable(TIMx);
    TMR_EnablePWMOutputs(TIMx);
	
	
}


/*!
 * @brief 初始化PWM
 */
void Drv_InitPWM(void)
{
	
	/*初始化PWM引脚*/
	pwm_gpio_config();
	
	
	if(PWM_OUTOUT_MAX==0){
	}else{
		/*配置PWMS输出模式*/
		Time_PWMOutputConfig(TMR1,4,10000,0);
		/*配置PWMS输出模式*/
		Time_PWMOutputConfig(TMR1,3,10000,0);
		/*配置PWMS输出模式*/
		Time_PWMOutputConfig(TMR1,2,10000,0);
		/*配置PWMS输出模式*/
		Time_PWMOutputConfig(TMR1,1,10000,0);
	}
	
}

/*!
 * @brief 设置输出PWM
 * @param[in] chanel		通道
 * @param[in] _ulFreq		频率
 * @param[in] _ulDutyCycle	占空比
 */
void Drv_Pwm_Set_Output(uint8_t chanel,uint32_t _ulFreq, uint32_t _ulDutyCycle)
{

	switch (chanel){
		case 0:
			Time_PWMOutputConfig(TMR1,4,_ulFreq,_ulDutyCycle);
			//TMR_ConfigCompare1(TMR1,_ulDutyCycle);
			break;
		case 1:
			Time_PWMOutputConfig(TMR1,3,_ulFreq,_ulDutyCycle);
			//TMR_ConfigCompare2(TMR1,_ulDutyCycle);
			break;
		case 2:
			Time_PWMOutputConfig(TMR1,2,_ulFreq,_ulDutyCycle);
			//TMR_ConfigCompare3(TMR1,_ulDutyCycle);
			break;
		case 3:
			Time_PWMOutputConfig(TMR1,1,_ulFreq,_ulDutyCycle);
			//TMR_ConfigCompare4(TMR1,_ulDutyCycle);
			break;
		default:break;
	}
	
}



/*!
 * @brief 测试PWM 8-2
 */
void Drv_PWM_Test(void)
{
	Drv_Pwm_Set_Output(0,10000,5000);
	Drv_Pwm_Set_Output(1,10000,6000);
	Drv_Pwm_Set_Output(2,10000,7000);
	Drv_Pwm_Set_Output(3,10000,8000);
//	TMR_ConfigCompare1(TMR1,2000);
//	TMR_ConfigCompare2(TMR1,3000);
//	TMR_ConfigCompare3(TMR1,4000);
//	TMR_ConfigCompare4(TMR1,5000);
}




/***************************** (C) COPYRIGHT 2023 Co-Win Hydrogen Power (Guangdong) Co., Ltd (文件结束) *********************************/
