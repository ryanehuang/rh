/*******************************************************************************
 *                            APPLICATION CODE
 *
 *(C) Copyright 2023; Co-Win Hydrogen Power (Guangdong) Co., Ltd
 *
 * @file    drv_spi_bus.c
 * @brief   SPI总线底层驱动。提供SPI配置、收发数据、多设备共享SPI支持。通过宏切换是硬件SPI还是软件模拟
 * @author  XieXunjie
 * @version 0.0.1
 * @date    2023-01-13
 *
 * -----------------------------------------------------------------------------
 *
 * -----------------------------------------------------------------------------
 * 文件修改历史：
 * <时间>           | <版本>    | <作者>        | <描述>
 * <2023-01-13>     | v0.0.1    | XieXunjie     | 模板文件
 * -----------------------------------------------------------------------------MAX6675_CSSPI_CS
 ******************************************************************************/

/*
********************************************************************************
*                              头文件包含
********************************************************************************
*/
#include "drv_spi_bus.h"
#include "user_config.h"

/*
********************************************************************************
*                              	本地宏指令定义 xiezongniubi
********************************************************************************
*/

#if 0
//开启SPI功能
#define		SPI1_ENABLE			1
#define		SPI2_ENABLE			1
//开启重映射
#define		SPI1_PinRemapConfig	1
#define		SPI2_PinRemapConfig	0


#if (SPI1_ENABLE==1)

#define 	SPI1_SCK_CLK 					RCM_APB2_PERIPH_GPIOD
#define		SPI1_SCK_PORT					GPIOD
#define		CAN1_RX_PIN						GPIO_PIN_0


#define SPI1_MISO_PORT  					GET_PIN(B, 4)
#define SPI1_MOSI_PORT  					GET_PIN(B, 5)

/*SPI时钟*/
#define SetSPI1_SCKMode()     	   			pin_mode(SPI1_SCK_PORT,PIN_MODE_AF_PP)
/*SPI 主机输入，从机输出*/
#define SetSPI1_MISOMode()    	       		pin_mode(SPI1_MISO_PORT,PIN_MODE_AF_PP)
/*SPI 主机输出，从机输入*/
#define SetSPI1_MOSIMode()    	       		pin_mode(SPI1_MOSI_PORT,PIN_MODE_AF_PP)
#endif

#if (SPI2_ENABLE==1)

#define SPI2_SCK_PORT  						GET_PIN(B, 13)
#define SPI2_MISO_PORT  					GET_PIN(B, 14)
#define SPI2_MOSI_PORT  					0

/*SPI时钟*/
#define SetSPI2_SCKMode()     	   			pin_mode(SPI2_SCK_PORT,PIN_MODE_AF_PP)
/*SPI 主机输入，从机输出*/
#define SetSPI2_MISOMode()    	       		pin_mode(SPI2_MISO_PORT,PIN_MODE_AF_PP)
/*SPI 主机输出，从机输入*/
#define SetSPI2_MOSIMode()    	       		pin_mode(SPI2_MOSI_PORT,PIN_MODE_AF_PP)

#endif

#endif

// 定义GPIO引脚
#define		SPI_CLK_PIN 		GPIO_PIN_10
#define		SPI_MISO_PIN		GPIO_PIN_12

#define		SPI_MOSI_PIN		GPIO_PIN_15

// 定义GPIO端口
#define		SPI_GPIO_PORT		GPIOE

// 定义SPI时钟速度
#define		SPI_CLOCK_SPEED		1000000 // 1MHz

/*
********************************************************************************
*                              	局部变量
********************************************************************************
*/


/*
********************************************************************************
*                               全局变量
********************************************************************************
*/

/*
********************************************************************************
*                             	函数声明
********************************************************************************
*/




/*
********************************************************************************
*                              	函数定义
********************************************************************************
*/

// 初始化SPI引脚
void SPI_GPIO_Init(void)
{
    GPIO_Config_T GPIO_InitStructure;

    // 使能GPIO时钟
	RCM_EnableAPB2PeriphClock(RCM_APB2_PERIPH_GPIOE);

    // 配置SPI时钟引脚
    GPIO_InitStructure.pin = SPI_CLK_PIN;
    GPIO_InitStructure.mode = GPIO_MODE_OUT_PP;
    GPIO_InitStructure.speed = GPIO_SPEED_50MHz;
	GPIO_Config(SPI_GPIO_PORT, &GPIO_InitStructure);

    // 配置SPI MISO引脚
    GPIO_InitStructure.pin = SPI_MISO_PIN;
    GPIO_InitStructure.mode = GPIO_MODE_IN_FLOATING;
    GPIO_Config(SPI_GPIO_PORT, &GPIO_InitStructure);

    // 配置SPI MOSI引脚
    GPIO_InitStructure.pin = SPI_MOSI_PIN;
    GPIO_InitStructure.mode = GPIO_MODE_OUT_PP;
    GPIO_InitStructure.speed = GPIO_SPEED_50MHz;
    GPIO_Config(SPI_GPIO_PORT, &GPIO_InitStructure);
}

// 发送一个字节的数据
void SPI_SendByte(uint8_t data)
{
    uint8_t i;

    // 依次发送8位数据
    for (i = 0; i < 8; i++)
    {
        // 设置时钟引脚为低电平
        GPIO_ResetBit(SPI_GPIO_PORT, SPI_CLK_PIN);

        // 发送数据位
        if (data & 0x80)
        {
            GPIO_SetBit(SPI_GPIO_PORT, SPI_MISO_PIN);
        }
        else
        {
            GPIO_ResetBit(SPI_GPIO_PORT, SPI_MISO_PIN);
        }

        // 设置时钟引脚为高电平
        GPIO_SetBit(SPI_GPIO_PORT, SPI_CLK_PIN);

        // 左移一位，准备发送下一位数据
        data <<= 1;
    }
}

// 接收一个字节的数据
uint8_t SPI_ReceiveByte(void)
{
    uint8_t i;
    uint8_t data = 0;

    // 依次接收8位数据
    for (i = 0; i < 8; i++)
    {
        // 设置时钟引脚为低电平
        GPIO_ResetBit(SPI_GPIO_PORT, SPI_CLK_PIN);

        // 读取数据位
        if (GPIO_ReadInputBit(SPI_GPIO_PORT, SPI_MISO_PIN))
        {
            data |= 0x01;
        }

        // 设置时钟引脚为高电平
        GPIO_SetBit(SPI_GPIO_PORT, SPI_CLK_PIN);

        // 左移一位，准备接收下一位数据
        data <<= 1;
    }

    return data;
}


/*!
 * @brief 	SPI1_Init 初始化
 * @return  无
 */
void Drv_SPI1_Init(void)
{
	 // 初始化SPI引脚
    SPI_GPIO_Init();
	#if 1
    // 设置SPI时钟速度
	RCM_EnableAPB2PeriphClock(RCM_APB2_PERIPH_SPI1);
	
    SPI_Config_T spiConfig;
	
	SPI_ConfigStructInit(&spiConfig);
	/*设置SPI的数据大小:SPI发送接收8位帧结构*/
    spiConfig.length = SPI_DATA_LENGTH_8B;
	/*定义波特率预分频的值:波特率预分频值为256*/
    spiConfig.baudrateDiv = SPI_BAUDRATE_DIV_256;
    /* 设置SPI单向或者双向的数据模式:SPI设置为双线双向全双工 */
    spiConfig.direction = SPI_DIRECTION_2LINES_FULLDUPLEX;
    /* 指定数据传输从MSB位还是LSB位开始:数据传输从MSB位开始 */
    spiConfig.firstBit = SPI_FIRSTBIT_MSB;
    /* 设置SPI工作模式:设置为主SPI */
    spiConfig.mode = SPI_MODE_MASTER;
    /* 串行同步时钟的空闲状态为高电平 */
    spiConfig.polarity = SPI_CLKPOL_HIGH;
    /*  NSS信号由硬件（NSS管脚）还是软件（使用SSI位）管理:
		内部NSS信号有SSI位控制  */
    spiConfig.nss = SPI_NSS_SOFT;
    /* 串行同步时钟的第二个跳变沿（上升或下降）数据被采样 */
    spiConfig.phase = SPI_CLKPHA_2EDGE;
    /* SPI config */
    SPI_Config(SPI1, &spiConfig);

    SPI_ConfigDataSize(SPI1, SPI_DATA_LENGTH_8B);

    SPI_Enable(SPI1);
	
	SPI_ReadWriteByte(SPI1,0xff);//启动传输	
	#endif
}

/*!
 * @brief SPI读写字节
 *
 * 备注：
 * @param[uint8_t] TxData, 要写入的字节
 *
 * @return  读取到的字节
 */
uint8_t SPI_ReadWriteByte(SPI_T* SPIx,uint8_t TxData)
{		
	uint16_t retry=0;				 	
	while (SPI_I2S_ReadStatusFlag(SPIx, SPI_FLAG_TXBE) == RESET) //检查指定的SPI标志位设置与否:发送缓存空标志位
		{
		retry++;
		if(retry>10000)return 0;
		}			  
	SPI_I2S_TxData(SPIx, TxData); //通过外设SPIx发送一个数据
	retry=0;

	while (SPI_I2S_ReadStatusFlag(SPIx, SPI_FLAG_RXBNE) == RESET) //检查指定的SPI标志位设置与否:接受缓存非空标志位
		{
		retry++;
		if(retry>10000)return 0;
		}	  						    
	return SPI_I2S_RxData(SPIx); //返回通过SPIx最近接收的数据					    
}

// 读取MAX6675温度
float MAX6675_ReadTemperature(void)
{
    uint16_t rawValue;
    float temperature;

    // 使能片选引脚
    GPIO_ResetBit(SPI_GPIO_PORT, SPI_MOSI_PIN);

    // 发送读取命令
    SPI_SendByte(0x00);

    // 读取高字节
    rawValue = SPI_ReceiveByte() << 8;

    // 读取低字节
    rawValue |= SPI_ReceiveByte();

    // 禁用片选引脚
    GPIO_SetBit(SPI_GPIO_PORT, SPI_MOSI_PIN);

    // 计算温度
    temperature = (rawValue >> 3) * 0.25;

    return temperature;
}

#if 0
/*!
 * @brief 	SPI2_Init 初始化
 * @return  无
 */
void Drv_SPI1_Init(void)
{
	SPI_InitTypeDef  SPI_InitStructure;
	RCC_APB2PeriphClockCmd(	RCC_APB2Periph_SPI1,  ENABLE );//SPI2时钟使能
	
#if (SPI1_ENABLE==1)
	
	#if (SPI1_PinRemapConfig==1)
	/*SPI1重映射*/
	RCC_APB2PeriphClockCmd(	RCC_APB2Periph_GPIOB, ENABLE );//PORTB时钟使能 	
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable , ENABLE);//释放JTAG引脚
    GPIO_PinRemapConfig(GPIO_Remap_SPI1 , ENABLE);//释放JTAG引脚
	#endif
	
	/*初始化SPI1时钟*/
	SetSPI1_SCKMode();
	/*初始化SPI1主读*/
	SetSPI1_MISOMode();
	/*初始化SPI1从读*/
	SetSPI1_MOSIMode();
#endif	
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;  //设置SPI单向或者双向的数据模式:SPI设置为双线双向全双工
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;		//设置SPI工作模式:设置为主SPI
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;		//设置SPI的数据大小:SPI发送接收8位帧结构
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;		//串行同步时钟的空闲状态为高电平
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;//SPI_CPHA_1Edge;//SPI_CPHA_2Edge;	//串行同步时钟的第二个跳变沿（上升或下降）数据被采样
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;		//NSS信号由硬件（NSS管脚）还是软件（使用SSI位）管理:内部NSS信号有SSI位控制
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;		//定义波特率预分频的值:波特率预分频值为256
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;	//指定数据传输从MSB位还是LSB位开始:数据传输从MSB位开始
	SPI_InitStructure.SPI_CRCPolynomial = 7;	//CRC值计算的多项式
	SPI_Init(SPI1, &SPI_InitStructure);  //根据SPI_InitStruct中指定的参数初始化外设SPIx寄存器
 
	SPI_Cmd(SPI1, ENABLE); //使能SPI外设
	
	SPI_ReadWriteByte(SPI1,0xff);//启动传输	
}


/*!
 * @brief 	SPI2_Init 初始化
 * @return  无
 */
void Drv_SPI2_Init(void)
{
	
	SPI_InitTypeDef  SPI_InitStructure;
	RCC_APB1PeriphClockCmd(	RCC_APB1Periph_SPI2,  ENABLE );//SPI2时钟使能
	SPI_I2S_DeInit(SPI2);
	#if (SPI2_ENABLE==1)	
	/*初始化SPI2时钟*/
	SetSPI2_SCKMode();
	/*初始化SPI1主读*/
	SetSPI2_MISOMode();
	/*初始化SPI1从读*/
	//SetSPI2_MOSIMode();
	#endif	
	
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;  //设置SPI单向或者双向的数据模式:SPI设置为双线双向全双工
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;		//设置SPI工作模式:设置为主SPI
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;		//设置SPI的数据大小:SPI发送接收8位帧结构
	//SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;		//串行同步时钟的空闲状态为高电平
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;//SPI_CPHA_1Edge;//SPI_CPHA_2Edge;	//串行同步时钟的第二个跳变沿（上升或下降）数据被采样
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;		//NSS信号由硬件（NSS管脚）还是软件（使用SSI位）管理:内部NSS信号有SSI位控制
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;		//定义波特率预分频的值:波特率预分频值为256
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;	//指定数据传输从MSB位还是LSB位开始:数据传输从MSB位开始
	//SPI_InitStructure.SPI_CRCPolynomial = 7;	//CRC值计算的多项式
	SPI_Init(SPI2, &SPI_InitStructure);  //根据SPI_InitStruct中指定的参数初始化外设SPIx寄存器
	SPI_Cmd(SPI2, ENABLE); //使能SPI外设
	SPI_ReadWriteByte(SPI2,0xff);//启动传输	
}
/*!
 * @brief SPI读写字节
 *
 * 备注：
 * @param[uint8_t] TxData, 要写入的字节
 *
 * @return  读取到的字节
 */
uint8_t SPI_ReadWriteByte(SPI_TypeDef* SPIx,uint8_t TxData)
{		
	uint16_t retry=0;				 	
	while (SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_TXE) == RESET) //检查指定的SPI标志位设置与否:发送缓存空标志位
		{
		retry++;
		if(retry>10000)return 0;
		}			  
	SPI_I2S_SendData(SPIx, TxData); //通过外设SPIx发送一个数据
	retry=0;

	while (SPI_I2S_GetFlagStatus(SPIx, SPI_I2S_FLAG_RXNE) == RESET) //检查指定的SPI标志位设置与否:接受缓存非空标志位
		{
		retry++;
		if(retry>10000)return 0;
		}	  						    
	return SPI_I2S_ReceiveData(SPIx); //返回通过SPIx最近接收的数据					    
}

#endif

/*** (C) COPYRIGHT 2023 Co-Win Hydrogen Power (Guangdong) Co., Ltd (文件结束) ***/
