/*******************************************************************************
 *                            APPLICATION CODE
 *
 *(C) Copyright 2023; Co-Win Hydrogen Power (Guangdong) Co., Ltd
 *
 * @file    drv_adc.c
 * @brief   模拟量底层驱动
 * @author  XieXunjie
 * @version 0.0.1
 * @date    2023-01-13
 *
 * -----------------------------------------------------------------------------
 *
 * -----------------------------------------------------------------------------
 * 文件修改历史：
 * <时间>           | <版本>    | <作者>        | <描述>
 * <2023-01-13>     | v0.0.1    | XieXunjie     | 模板文件
 * -----------------------------------------------------------------------------
 ******************************************************************************/

/*
********************************************************************************
*                              头文件包含
********************************************************************************
*/
#include "drv_adc.h"
#include "user_config.h"

/*
********************************************************************************
*                              	本地宏指令定义
********************************************************************************
*/
/*总共32个IO*/
/*STM32F103C8T6风扇板*/
#if (Board_Choose==0)

/*STM32F103C8T6 IO拓展板*/
#elif (Board_Choose==1)

#elif (Board_Choose==2)
/*STM32F103VCT6 风扇板*/

#elif (Board_Choose==3)
/*STM32F103VCT6 动环板*/

#elif (Board_Choose==4)
/*APM32A103VET7 动环板*/


/* ADC1 data register address */
#define ADC_DR_ADDR         ((uint32_t)ADC1_BASE + 0x4C)
/* ADC通道 2+14*/
#define ADCx_CHANNEL_MAX 	16

/*MCU芯片温度*/
/*MCU芯片电压*/

#define		ADCx_CHANNEL_0_CLK						RCM_APB2_PERIPH_GPIOB
#define		ADCx_CHANNEL_0_PORT						GPIOB
#define		ADCx_CHANNEL_0_PIN						GPIO_PIN_0

#define		ADCx_CHANNEL_1_CLK						RCM_APB2_PERIPH_GPIOC
#define		ADCx_CHANNEL_1_PORT						GPIOC
#define		ADCx_CHANNEL_1_PIN						GPIO_PIN_5

#define		ADCx_CHANNEL_2_CLK						RCM_APB2_PERIPH_GPIOC
#define		ADCx_CHANNEL_2_PORT						GPIOC
#define		ADCx_CHANNEL_2_PIN						GPIO_PIN_4

#define		ADCx_CHANNEL_3_CLK						RCM_APB2_PERIPH_GPIOA
#define		ADCx_CHANNEL_3_PORT						GPIOA
#define		ADCx_CHANNEL_3_PIN						GPIO_PIN_7

#define		ADCx_CHANNEL_4_CLK						RCM_APB2_PERIPH_GPIOA
#define		ADCx_CHANNEL_4_PORT						GPIOA
#define		ADCx_CHANNEL_4_PIN						GPIO_PIN_6

#define		ADCx_CHANNEL_5_CLK						RCM_APB2_PERIPH_GPIOA
#define		ADCx_CHANNEL_5_PORT						GPIOA
#define		ADCx_CHANNEL_5_PIN						GPIO_PIN_5

#define		ADCx_CHANNEL_6_CLK						RCM_APB2_PERIPH_GPIOA
#define		ADCx_CHANNEL_6_PORT						GPIOA
#define		ADCx_CHANNEL_6_PIN						GPIO_PIN_4

#define		ADCx_CHANNEL_7_CLK						RCM_APB2_PERIPH_GPIOA
#define		ADCx_CHANNEL_7_PORT						GPIOA
#define		ADCx_CHANNEL_7_PIN						GPIO_PIN_3

#define		ADCx_CHANNEL_8_CLK						RCM_APB2_PERIPH_GPIOA
#define		ADCx_CHANNEL_8_PORT						GPIOA
#define		ADCx_CHANNEL_8_PIN						GPIO_PIN_2

#define		ADCx_CHANNEL_9_CLK						RCM_APB2_PERIPH_GPIOA
#define		ADCx_CHANNEL_9_PORT						GPIOA
#define		ADCx_CHANNEL_9_PIN						GPIO_PIN_1

#define		ADCx_CHANNEL_10_CLK						RCM_APB2_PERIPH_GPIOA
#define		ADCx_CHANNEL_10_PORT					GPIOA
#define		ADCx_CHANNEL_10_PIN						GPIO_PIN_0

#define		ADCx_CHANNEL_11_CLK						RCM_APB2_PERIPH_GPIOC
#define		ADCx_CHANNEL_11_PORT					GPIOC
#define		ADCx_CHANNEL_11_PIN						GPIO_PIN_3

#define		ADCx_CHANNEL_12_CLK						RCM_APB2_PERIPH_GPIOC
#define		ADCx_CHANNEL_12_PORT					GPIOC
#define		ADCx_CHANNEL_12_PIN						GPIO_PIN_2

#define		ADCx_CHANNEL_13_CLK						RCM_APB2_PERIPH_GPIOC
#define		ADCx_CHANNEL_13_PORT					GPIOC
#define		ADCx_CHANNEL_13_PIN						GPIO_PIN_1


#define		ADCx_CHANNEL_0							ADC_CHANNEL_16
#define		ADCx_CHANNEL_1							ADC_CHANNEL_17
#define		ADCx_CHANNEL_2							ADC_CHANNEL_8
#define		ADCx_CHANNEL_3							ADC_CHANNEL_15
#define		ADCx_CHANNEL_4							ADC_CHANNEL_14
#define		ADCx_CHANNEL_5							ADC_CHANNEL_7
#define		ADCx_CHANNEL_6							ADC_CHANNEL_6
#define		ADCx_CHANNEL_7							ADC_CHANNEL_5
#define		ADCx_CHANNEL_8							ADC_CHANNEL_4
#define		ADCx_CHANNEL_9							ADC_CHANNEL_3
#define		ADCx_CHANNEL_10							ADC_CHANNEL_2
#define		ADCx_CHANNEL_11							ADC_CHANNEL_1
#define		ADCx_CHANNEL_12							ADC_CHANNEL_0
#define		ADCx_CHANNEL_13							ADC_CHANNEL_13
#define		ADCx_CHANNEL_14							ADC_CHANNEL_12
#define		ADCx_CHANNEL_15							ADC_CHANNEL_11
#define		ADCx_CHANNEL_16							0
#define		ADCx_CHANNEL_17							0
#define		ADCx_CHANNEL_18							0
#define		ADCx_CHANNEL_19							0
#define		ADCx_CHANNEL_20							0
#define		ADCx_CHANNEL_21							0
#define		ADCx_CHANNEL_22							0
#define		ADCx_CHANNEL_23							0



const uint16_t ADC_CHANNEL_CLK[ADCx_CHANNEL_MAX] = { 
												ADCx_CHANNEL_0_CLK	,		ADCx_CHANNEL_1_CLK,
												ADCx_CHANNEL_2_CLK	,		ADCx_CHANNEL_3_CLK,
												ADCx_CHANNEL_4_CLK	,		ADCx_CHANNEL_5_CLK,
												ADCx_CHANNEL_6_CLK	,		ADCx_CHANNEL_7_CLK,
												ADCx_CHANNEL_8_CLK	,		ADCx_CHANNEL_9_CLK,
												ADCx_CHANNEL_10_CLK	,		ADCx_CHANNEL_11_CLK,
												ADCx_CHANNEL_12_CLK	,		ADCx_CHANNEL_13_CLK,
										  };
GPIO_T* ADC_CHANNEL_PORT[ADCx_CHANNEL_MAX] =  {
												ADCx_CHANNEL_0_PORT	,		ADCx_CHANNEL_1_PORT,
												ADCx_CHANNEL_2_PORT	,		ADCx_CHANNEL_3_PORT,
												ADCx_CHANNEL_4_PORT	,		ADCx_CHANNEL_5_PORT,
												ADCx_CHANNEL_6_PORT	,		ADCx_CHANNEL_7_PORT,
												ADCx_CHANNEL_8_PORT	,		ADCx_CHANNEL_9_PORT,
												ADCx_CHANNEL_10_PORT	,		ADCx_CHANNEL_11_PORT,
												ADCx_CHANNEL_12_PORT	,		ADCx_CHANNEL_13_PORT,
										  };
const uint16_t ADC_CHANNEL_PIN[ADCx_CHANNEL_MAX] = {
												ADCx_CHANNEL_0_PIN	,		ADCx_CHANNEL_1_PIN,
												ADCx_CHANNEL_2_PIN	,		ADCx_CHANNEL_3_PIN,
												ADCx_CHANNEL_4_PIN	,		ADCx_CHANNEL_5_PIN,
												ADCx_CHANNEL_6_PIN	,		ADCx_CHANNEL_7_PIN,
												ADCx_CHANNEL_8_PIN	,		ADCx_CHANNEL_9_PIN,
												ADCx_CHANNEL_10_PIN	,		ADCx_CHANNEL_11_PIN,
												ADCx_CHANNEL_12_PIN	,		ADCx_CHANNEL_13_PIN,
										  };

										  
#define __ADC_CHANNEL_Gather_(x)  				ADCx_CHANNEL_##x
#define	ADC_CHANNEL_Gather_(x)				x==0?__ADC_CHANNEL_Gather_(0): \
											x==1?__ADC_CHANNEL_Gather_(1): \
											x==2?__ADC_CHANNEL_Gather_(2): \
											x==3?__ADC_CHANNEL_Gather_(3): \
											x==4?__ADC_CHANNEL_Gather_(4): \
											x==5?__ADC_CHANNEL_Gather_(5): \
											x==6?__ADC_CHANNEL_Gather_(6): \
											x==7?__ADC_CHANNEL_Gather_(7): \
											x==8?__ADC_CHANNEL_Gather_(8): \
											x==9?__ADC_CHANNEL_Gather_(9): \
											x==10?__ADC_CHANNEL_Gather_(10): \
											x==11?__ADC_CHANNEL_Gather_(11): \
											x==12?__ADC_CHANNEL_Gather_(12): \
											x==13?__ADC_CHANNEL_Gather_(13): \
											x==14?__ADC_CHANNEL_Gather_(14): \
											x==15?__ADC_CHANNEL_Gather_(15): \
											x==16?__ADC_CHANNEL_Gather_(16): \
											x==17?__ADC_CHANNEL_Gather_(17): \
											x==18?__ADC_CHANNEL_Gather_(18): \
											x==19?__ADC_CHANNEL_Gather_(19): \
											x==20?__ADC_CHANNEL_Gather_(20): \
											x==21?__ADC_CHANNEL_Gather_(21): \
											x==22?__ADC_CHANNEL_Gather_(22): \
											x==23?__ADC_CHANNEL_Gather_(23): \
											__ADC_CHANNEL_Gather_(23)
											
#endif

/*
********************************************************************************
*                              	局部变量
********************************************************************************
*/


/*
********************************************************************************
*                               全局变量
********************************************************************************
*/
__IO uint16_t ADC_ConvertedValue[ADCx_CHANNEL_MAX]={0};

/*
********************************************************************************
*                             	函数声明
********************************************************************************
*/




/*
********************************************************************************
*                              	函数定义
********************************************************************************
*/
static void SetADCx_GPIOMode(uint8_t digital_output)
{
    GPIO_Config_T  configStruct;
	
    RCM_EnableAPB2PeriphClock(ADC_CHANNEL_CLK[digital_output]);

	/* Configure PC0 (ADC Channel0) as analog input */
    GPIO_ConfigStructInit(&configStruct);
	configStruct.pin     = ADC_CHANNEL_PIN[digital_output];
    configStruct.mode    = GPIO_MODE_ANALOG;
    GPIO_Config(ADC_CHANNEL_PORT[digital_output], &configStruct);
	
}

/*!
 * @brief 	模拟量GPIO初始化
 * @return  无
 */
static void ADCx_GPIO_Config(void)
{
	uint8_t i=0;
	if(ADCx_CHANNEL_MAX==0){
	}else{
		for (i=0;i<ADCx_CHANNEL_MAX;i++){
			/*初始化模拟量模式*/
			SetADCx_GPIOMode(i);
		}	
	}		
}
/**
  * @brief  配置ADC使用的DMA
  * @param  无
  * @retval 无
  */
static void ADCx_DMA_Config(void)
{
	
	DMA_Config_T dmaConfig;

    /* Enable DMA Clock */
    RCM_EnableAHBPeriphClock(RCM_AHB_PERIPH_DMA1);

    /* DMA config */
    dmaConfig.peripheralBaseAddr = ADC_DR_ADDR;
    dmaConfig.memoryBaseAddr = (uint32_t)&ADC_ConvertedValue;
    dmaConfig.dir = DMA_DIR_PERIPHERAL_SRC;
    dmaConfig.bufferSize = ADCx_CHANNEL_MAX;
    dmaConfig.peripheralInc = DMA_PERIPHERAL_INC_DISABLE;
    dmaConfig.memoryInc = DMA_MEMORY_INC_ENABLE;//DMA_PeripheralInc_Disable
    dmaConfig.peripheralDataSize = DMA_PERIPHERAL_DATA_SIZE_HALFWORD;
    dmaConfig.memoryDataSize = DMA_MEMORY_DATA_SIZE_HALFWORD;
    dmaConfig.loopMode = DMA_MODE_CIRCULAR;
    dmaConfig.priority = DMA_PRIORITY_HIGH;
    dmaConfig.M2M = DMA_M2MEN_DISABLE;
    
    /* Enable DMA channel */
    DMA_Config(DMA1_Channel1, &dmaConfig);
    
    /* Enable DMA */
    DMA_Enable(DMA1_Channel1);
	
}

/**
  * @brief  配置ADC工作模式
  * @param  无
  * @retval 无
  */
static void ADCx_Mode_Config(void)
{
	uint8_t i=0;
	ADC_Config_T       		adcConfig;
	
	 /* ADCCLK = PCLK2/4 */
    RCM_ConfigADCCLK(RCM_PCLK2_DIV_4);
    
    /* Enable ADC clock */
    RCM_EnableAPB2PeriphClock(RCM_APB2_PERIPH_ADC1);

    /* ADC configuration */
    ADC_Reset(ADC1);

    ADC_ConfigStructInit(&adcConfig);
    adcConfig.mode                  = ADC_MODE_INDEPENDENT;
    adcConfig.scanConvMode          = ENABLE;
    adcConfig.continuosConvMode     = ENABLE;
    adcConfig.externalTrigConv      = ADC_EXT_TRIG_CONV_None;
    adcConfig.dataAlign             = ADC_DATA_ALIGN_RIGHT;
    /* channel number */
    adcConfig.nbrOfChannel          = ADCx_CHANNEL_MAX;
    ADC_Config(ADC1, &adcConfig);
    
	/*开启ADC1内部温度传感器*/
	ADC_EnableTempSensorVrefint(ADC1);
    /* ADC channel Convert configuration */
	/*DMA模式 获取通道模拟量*/
	for (i=0;i<ADCx_CHANNEL_MAX;i++){
		ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_Gather_(i), i+1, ADC_SAMPLETIME_13CYCLES5);
	}
	
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_8, 1, ADC_SAMPLETIME_13CYCLES5);
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_15, 2, ADC_SAMPLETIME_13CYCLES5);
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_14, 3, ADC_SAMPLETIME_13CYCLES5);
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_7, 4, ADC_SAMPLETIME_13CYCLES5);
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_6, 5, ADC_SAMPLETIME_13CYCLES5);
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_5, 6, ADC_SAMPLETIME_13CYCLES5);
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_4, 7, ADC_SAMPLETIME_13CYCLES5);
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_3, 8, ADC_SAMPLETIME_13CYCLES5);
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_2, 9, ADC_SAMPLETIME_13CYCLES5);
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_1, 10, ADC_SAMPLETIME_13CYCLES5);
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_0, 11, ADC_SAMPLETIME_13CYCLES5);
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_13, 12, ADC_SAMPLETIME_13CYCLES5);
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_12, 13, ADC_SAMPLETIME_13CYCLES5);
//	ADC_ConfigRegularChannel(ADC1, ADC_CHANNEL_11, 14, ADC_SAMPLETIME_13CYCLES5);


    /* Enable the external trigger inject configuration */
	
    /* Enable ADC DMA */
    ADC_EnableDMA(ADC1);

    /* Enable ADC */
    ADC_Enable(ADC1);

    /* Enable ADC1 reset calibration register */
    ADC_ResetCalibration(ADC1);
    /* Check the end of ADC1 reset calibration register */
    while(ADC_ReadResetCalibrationStatus(ADC1));

    /* Start ADC1 calibration */
    ADC_StartCalibration(ADC1);
    /* Check the end of ADC1 calibration */
    while(ADC_ReadCalibrationStartFlag(ADC1));

    /* Start ADC1 Software Conversion */
    ADC_EnableSoftwareStartConv(ADC1);
}


/**
  * @brief  ADC初始化
  * @param  无
  * @retval 无
  */
void Drv_InitADCx(void)
{
	/*GPIO配置初始化*/
		ADCx_GPIO_Config();
	/*DMA配置初始化*/
		ADCx_DMA_Config();
	/*ADC模式初始化*/
		ADCx_Mode_Config();
}
/**
  * @brief  读取ADC值
  * @param  无
  * @retval ADC值
  */
uint16_t GetAdcValue(uint16_t index){
	
	if(ADCx_CHANNEL_MAX==0){
		return 0;
	}else{
		return ADC_ConvertedValue[index];
	}		
}

/**
  * @brief  ADC测试
  * @param  无
  * @retval 无
  */
void Drv_adc_test(void)
{
	/*电压信号	3973*/
//	int i;
//	float temp_value=((1.43f - ADC_ConvertedValue[0]*(3.3f/4096)) * 1000 / 4.3f + 25);
//	float Volt_value=(ADC_ConvertedValue[1]*3.3f)/4095.0f;
//	printf("\r\n====================\r\n");
//	for(i=0;i<ADCx_CHANNEL_MAX;i++){
//		printf("ADC_ConvertedValue[%d]:%d \r\n",i,ADC_ConvertedValue[i]);
//	}
//	printf("ADC_TEMP:%f \r\n",temp_value);
//	printf("Volt_value:%f \r\n",Volt_value);
	
	
	float Volt_value12=(ADC_ConvertedValue[12]*5.0f)/4020.0f*100.0;
	float Volt_value13=(ADC_ConvertedValue[13]*5.0f)/4020.0f*100.0;
	float Volt_value14=(ADC_ConvertedValue[14]*5.0f)/4020.0f*10.0;
	float Volt_value15=(ADC_ConvertedValue[15]*5.0f)/4020.0f*10.0;
	
	
//	printf("12_ADC_:==%d \r\r\n",ADC_ConvertedValue[12]);
	printf("12_Volt_:==  %0.2f \r\r\n",Volt_value12);
	printf("\r\n");
	
//	printf("13_ADC_:==%d \r\r\n",ADC_ConvertedValue[13]);
	printf("13_Volt_:==  %0.2f \r\r\n",Volt_value13);
	
	printf("12-13△△△△:==  %0.2f \r\r\n",Volt_value12-Volt_value13);
	printf("\r\n");
	
//	printf("14_ADC_:==%d \r\r\n",ADC_ConvertedValue[14]);
	printf("14_Volt_:==  %0.2f \r\r\n",Volt_value14);
	printf("\r\n");
	
//	printf("15_ADC_:==%d \r\r\n",ADC_ConvertedValue[15]);
	printf("15_Volt_:==  %0.2f \r\r\n",Volt_value15);
	printf("14-15△△△△:==  %0.2f \r\r\n",Volt_value14-Volt_value15);
	printf("\r\n");
	
	
	
	
}

/*** (C) COPYRIGHT 2023 Co-Win Hydrogen Power (Guangdong) Co., Ltd (文件结束) ***/
