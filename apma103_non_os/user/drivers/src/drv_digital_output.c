/*******************************************************************************
 *                            APPLICATION CODE
 *
 *(C) Copyright 2023; Co-Win Hydrogen Power (Guangdong) Co., Ltd
 *
 * @file    drv_digital_output.c
 * @brief   数字输出IO驱动
 * @author  XieXunjie
 * @version 0.0.2
 * @date    2023-03-02
 *
 * -----------------------------------------------------------------------------
 *
 * -----------------------------------------------------------------------------
 * 文件修改历史：
 * <时间>           | <版本>    | <作者>        | <描述>
 * <2023-03-02>     | v0.0.2    | XieXunjie     | 数字输出IO驱动
 * <2023-01-13>     | v0.0.1    | XieXunjie     | 模板文件
 * -----------------------------------------------------------------------------
 ******************************************************************************/

/*
********************************************************************************
*                              包含头文件
********************************************************************************
*/

#include "drv_digital_output.h"	
#include "user_config.h"

/*
********************************************************************************
*                              	本地宏指令定义
********************************************************************************
*/
/*总共32个IO*/
/*STM32F103C8T6风扇板*/
#if (Board_Choose==0)

/*STM32F103C8T6 IO拓展板*/
#elif (Board_Choose==1)

#elif (Board_Choose==2)
/*STM32F103VCT6 风扇板*/

#elif (Board_Choose==3)
/*STM32F103VCT6 动环板*/

#elif (Board_Choose==4)
/*APM32A103VET7 动环板*/

#define DIGITAL_OUTOUT_MAX                      20U

//板载状态指示灯 PE8
#define DIGITAL_OUTPUT_0_CLK					RCM_APB2_PERIPH_GPIOE
#define DIGITAL_OUTPUT_0_PORT					GPIOE
#define DIGITAL_OUTPUT_0_PIN					GPIO_PIN_8

//SD ERR PA8
#define DIGITAL_OUTPUT_1_CLK					RCM_APB2_PERIPH_GPIOA
#define DIGITAL_OUTPUT_1_PORT                 	GPIOA
#define DIGITAL_OUTPUT_1_PIN					GPIO_PIN_8

//SD OK PC7
#define DIGITAL_OUTPUT_2_CLK    				RCM_APB2_PERIPH_GPIOC
#define DIGITAL_OUTPUT_2_PORT    				GPIOC
#define DIGITAL_OUTPUT_2_PIN					GPIO_PIN_7

//SD BLUE PC6
#define DIGITAL_OUTPUT_3_CLK					RCM_APB2_PERIPH_GPIOC
#define DIGITAL_OUTPUT_3_PORT					GPIOC
#define DIGITAL_OUTPUT_3_PIN					GPIO_PIN_6

//蜂鸣器 PE7
#define DIGITAL_OUTPUT_4_CLK					RCM_APB2_PERIPH_GPIOE
#define DIGITAL_OUTPUT_4_PORT					GPIOE
#define DIGITAL_OUTPUT_4_PIN					GPIO_PIN_7


/*24V 高边开关1-9 PC0 PE6-0 PB9 */
#define DIGITAL_OUTPUT_5_CLK					RCM_APB2_PERIPH_GPIOC
#define DIGITAL_OUTPUT_5_PORT					GPIOC
#define DIGITAL_OUTPUT_5_PIN					GPIO_PIN_0

#define DIGITAL_OUTPUT_6_CLK					RCM_APB2_PERIPH_GPIOE
#define DIGITAL_OUTPUT_6_PORT					GPIOE
#define DIGITAL_OUTPUT_6_PIN					GPIO_PIN_6

#define DIGITAL_OUTPUT_7_CLK					RCM_APB2_PERIPH_GPIOE
#define DIGITAL_OUTPUT_7_PORT					GPIOE
#define DIGITAL_OUTPUT_7_PIN					GPIO_PIN_5

#define DIGITAL_OUTPUT_8_CLK					RCM_APB2_PERIPH_GPIOE
#define DIGITAL_OUTPUT_8_PORT					GPIOE
#define DIGITAL_OUTPUT_8_PIN					GPIO_PIN_4

#define DIGITAL_OUTPUT_9_CLK					RCM_APB2_PERIPH_GPIOE
#define DIGITAL_OUTPUT_9_PORT					GPIOE
#define DIGITAL_OUTPUT_9_PIN					GPIO_PIN_3

#define DIGITAL_OUTPUT_10_CLK					RCM_APB2_PERIPH_GPIOE
#define DIGITAL_OUTPUT_10_PORT					GPIOE
#define DIGITAL_OUTPUT_10_PIN					GPIO_PIN_2

#define DIGITAL_OUTPUT_11_CLK					RCM_APB2_PERIPH_GPIOE
#define DIGITAL_OUTPUT_11_PORT					GPIOE
#define DIGITAL_OUTPUT_11_PIN					GPIO_PIN_1

#define DIGITAL_OUTPUT_12_CLK					RCM_APB2_PERIPH_GPIOE
#define DIGITAL_OUTPUT_12_PORT					GPIOE
#define DIGITAL_OUTPUT_12_PIN					GPIO_PIN_0

#define DIGITAL_OUTPUT_13_CLK					RCM_APB2_PERIPH_GPIOB
#define DIGITAL_OUTPUT_13_PORT					GPIOB
#define DIGITAL_OUTPUT_13_PIN					GPIO_PIN_9

/*外置LED 1-2 PD4-3 */

#define DIGITAL_OUTPUT_14_CLK					RCM_APB2_PERIPH_GPIOD
#define DIGITAL_OUTPUT_14_PORT					GPIOD
#define DIGITAL_OUTPUT_14_PIN					GPIO_PIN_4

#define DIGITAL_OUTPUT_15_CLK					RCM_APB2_PERIPH_GPIOD
#define DIGITAL_OUTPUT_15_PORT					GPIOD
#define DIGITAL_OUTPUT_15_PIN					GPIO_PIN_3

/*PWM供电 1-4 PD8-11 */

#define DIGITAL_OUTPUT_16_CLK					RCM_APB2_PERIPH_GPIOD
#define DIGITAL_OUTPUT_16_PORT					GPIOD
#define DIGITAL_OUTPUT_16_PIN					GPIO_PIN_8

#define DIGITAL_OUTPUT_17_CLK					RCM_APB2_PERIPH_GPIOD
#define DIGITAL_OUTPUT_17_PORT					GPIOD
#define DIGITAL_OUTPUT_17_PIN					GPIO_PIN_9

#define DIGITAL_OUTPUT_18_CLK					RCM_APB2_PERIPH_GPIOD
#define DIGITAL_OUTPUT_18_PORT					GPIOD
#define DIGITAL_OUTPUT_18_PIN					GPIO_PIN_10

#define DIGITAL_OUTPUT_19_CLK					RCM_APB2_PERIPH_GPIOD
#define DIGITAL_OUTPUT_19_PORT					GPIOD
#define DIGITAL_OUTPUT_19_PIN					GPIO_PIN_11


/*APM32F103VCT6 动环板*/
const uint32_t GPIO_CLK[DIGITAL_OUTOUT_MAX] = { 
											DIGITAL_OUTPUT_0_CLK	,	DIGITAL_OUTPUT_1_CLK,
											DIGITAL_OUTPUT_2_CLK	,	DIGITAL_OUTPUT_3_CLK,
											DIGITAL_OUTPUT_4_CLK	,	DIGITAL_OUTPUT_5_CLK,
											DIGITAL_OUTPUT_6_CLK	,	DIGITAL_OUTPUT_7_CLK,
											DIGITAL_OUTPUT_8_CLK	,	DIGITAL_OUTPUT_9_CLK,
											DIGITAL_OUTPUT_10_CLK	,	DIGITAL_OUTPUT_11_CLK,
											DIGITAL_OUTPUT_12_CLK	,	DIGITAL_OUTPUT_13_CLK,
											DIGITAL_OUTPUT_14_CLK 	,	DIGITAL_OUTPUT_15_CLK,
											DIGITAL_OUTPUT_16_CLK 	,	DIGITAL_OUTPUT_17_CLK,
											DIGITAL_OUTPUT_18_CLK 	,	DIGITAL_OUTPUT_19_CLK,
										  };
GPIO_T* GPIO_PORT[DIGITAL_OUTOUT_MAX] =  {
											DIGITAL_OUTPUT_0_PORT	,	DIGITAL_OUTPUT_1_PORT,
											DIGITAL_OUTPUT_2_PORT	,	DIGITAL_OUTPUT_3_PORT,
											DIGITAL_OUTPUT_4_PORT	,	DIGITAL_OUTPUT_5_PORT,
											DIGITAL_OUTPUT_6_PORT	,	DIGITAL_OUTPUT_7_PORT,
											DIGITAL_OUTPUT_8_PORT	,	DIGITAL_OUTPUT_9_PORT,
											DIGITAL_OUTPUT_10_PORT	,	DIGITAL_OUTPUT_11_PORT,
											DIGITAL_OUTPUT_12_PORT	,	DIGITAL_OUTPUT_13_PORT,
											DIGITAL_OUTPUT_14_PORT 	,	DIGITAL_OUTPUT_15_PORT,
											DIGITAL_OUTPUT_16_PORT 	,	DIGITAL_OUTPUT_17_PORT,
											DIGITAL_OUTPUT_18_PORT 	,	DIGITAL_OUTPUT_19_PORT,
										  };
const uint16_t GPIO_PIN[DIGITAL_OUTOUT_MAX] = {
											DIGITAL_OUTPUT_0_PIN	,	DIGITAL_OUTPUT_1_PIN,
											DIGITAL_OUTPUT_2_PIN	,	DIGITAL_OUTPUT_3_PIN,
											DIGITAL_OUTPUT_4_PIN	,	DIGITAL_OUTPUT_5_PIN,
											DIGITAL_OUTPUT_6_PIN	,	DIGITAL_OUTPUT_7_PIN,
											DIGITAL_OUTPUT_8_PIN	,	DIGITAL_OUTPUT_9_PIN,
											DIGITAL_OUTPUT_10_PIN	,	DIGITAL_OUTPUT_11_PIN,
											DIGITAL_OUTPUT_12_PIN	,	DIGITAL_OUTPUT_13_PIN,
											DIGITAL_OUTPUT_14_PIN 	,	DIGITAL_OUTPUT_15_PIN,
											DIGITAL_OUTPUT_16_PIN 	,	DIGITAL_OUTPUT_17_PIN,
											DIGITAL_OUTPUT_18_PIN 	,	DIGITAL_OUTPUT_19_PIN,
										  };

#else

#endif



/*
********************************************************************************
*                              	局部变量
********************************************************************************
*/


/*
********************************************************************************
*                               全局变量
********************************************************************************
*/

/*
********************************************************************************
*                             	函数声明
********************************************************************************
*/




/*
********************************************************************************
*                              	函数定义
********************************************************************************
*/
static void SetDoMode(uint8_t digital_output)
{
    GPIO_Config_T  configStruct;

    /* Enable the GPIO_LED Clock */
    RCM_EnableAPB2PeriphClock(GPIO_CLK[digital_output]);

    /* Configure the GPIO_LED pin */
    configStruct.pin = GPIO_PIN[digital_output];
    configStruct.mode = GPIO_MODE_OUT_PP;
    configStruct.speed = GPIO_SPEED_50MHz;

    GPIO_Config(GPIO_PORT[digital_output], &configStruct);
    GPIO_PORT[digital_output]->BC = GPIO_PIN[digital_output];
}

void SetDoOut(uint8_t digital_output,uint8_t level)
{
	if(level==PIN_LOW){
		GPIO_PORT[digital_output]->BC = GPIO_PIN[digital_output];
		
	}else if(level==PIN_HIGH){
		GPIO_PORT[digital_output]->BSC = GPIO_PIN[digital_output];
	}else if(level==PIN_REVERSAL){
		GPIO_PORT[digital_output]->ODATA ^= GPIO_PIN[digital_output];
	}
    
}

/*!
 * @brief 	数字输出IO驱动初始化
 * @return  无
 */
void Drv_DigtalOutputInit( void)
{
	int i=0;
	/*设置数字io输出模式*/
	for (i=0;i<DIGITAL_OUTOUT_MAX;i++){
		/*初始化GPIO输出模式*/
		SetDoMode(i);
		/*关闭引脚：输出低电平*/
		SetDoOut(i,PIN_LOW);
		/*0-4 系统指示灯*/
		SetDoOut(0,PIN_HIGH);
		SetDoOut(1,PIN_HIGH);
		SetDoOut(2,PIN_HIGH);
		SetDoOut(3,PIN_HIGH);
	}
}


/*!
 * @brief 数字输出IO控制
 * @param[ uint8_t ] pin    引脚号
 * @param[ uint8_t ] sta    控制状态
 */
void Drv_SetDigitalOut(uint8_t pin,uint8_t sta)
{
		/* 开关判断 */
	if(sta<=PIN_REVERSAL){
		/*0-31 板载io*/
		if(pin<=31){
			SetDoOut(pin,sta);
		}
	}
}

/*!
 * @brief 测试数字IO控制
 */
void Drv_Gpio_Test(void)
{
	int i=5;
	/*设置数字io输出模式*/
	for (i=5;i<DIGITAL_OUTOUT_MAX;i++){
		Drv_SetDigitalOut(i,PIN_HIGH);
	}
}


/***************************** (C) COPYRIGHT 2023 Co-Win Hydrogen Power (Guangdong) Co., Ltd (文件结束) *********************************/
