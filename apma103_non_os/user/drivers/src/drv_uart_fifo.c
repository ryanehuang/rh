/*******************************************************************************
 *                            APPLICATION CODE
 *
 *(C) Copyright 2023; Co-Win Hydrogen Power (Guangdong) Co., Ltd
 *
 * @file    drv_uart_fifo.c
 * @brief   串口底层驱动
 * @author  XieXunjie
 * @version 0.0.2
 * @date    2023-03-02
 *
 * -----------------------------------------------------------------------------
 *
 * -----------------------------------------------------------------------------
 * 文件修改历史：
 * <时间>           | <版本>    | <作者>        | <描述>
 * <2023-03-02>     | v0.0.2    | XieXunjie     | 串口底层驱动
 * <2023-01-13>     | v0.0.1    | XieXunjie     | 模板文件
 * -----------------------------------------------------------------------------
 ******************************************************************************/

/*
********************************************************************************
*                              头文件包含
********************************************************************************
*/
#include "drv_uart_fifo.h"
#include "user_config.h"


/*
********************************************************************************
*                              	本地宏指令定义
********************************************************************************
*/
#define COMn                            3

/**
 * @brief Definition for COM port2, connected to USART2
 */
 //PD 5 6 USARt2 复用
#define MINI_COM2                        USART2
#define MINI_COM2_CLK                    RCM_APB1_PERIPH_USART2
#define MINI_COM2_TX_PIN                 GPIO_PIN_5
#define MINI_COM2_TX_GPIO_PORT           GPIOD
#define MINI_COM2_TX_GPIO_CLK            RCM_APB2_PERIPH_GPIOD
#define MINI_COM2_RX_PIN                 GPIO_PIN_6
#define MINI_COM2_RX_GPIO_PORT           GPIOD
#define MINI_COM2_RX_GPIO_CLK            RCM_APB2_PERIPH_GPIOD
#define MINI_COM2_IRQn                   USART2_IRQn

/**
 * @brief Definition for COM port1, connected to USART1
 */
#define MINI_COM1                        USART1
#define MINI_COM1_CLK                    RCM_APB2_PERIPH_USART1
#define MINI_COM1_TX_PIN                 GPIO_PIN_9
#define MINI_COM1_TX_GPIO_PORT           GPIOA
#define MINI_COM1_TX_GPIO_CLK            RCM_APB2_PERIPH_GPIOA
#define MINI_COM1_RX_PIN                 GPIO_PIN_10
#define MINI_COM1_RX_GPIO_PORT           GPIOA
#define MINI_COM1_RX_GPIO_CLK            RCM_APB2_PERIPH_GPIOA
#define MINI_COM1_IRQn                   USART1_IRQn



//PB10 11 usart
#define MINI_COM3                        USART3
#define MINI_COM3_CLK                    RCM_APB1_PERIPH_USART3
#define MINI_COM3_TX_PIN                 GPIO_PIN_10
#define MINI_COM3_TX_GPIO_PORT           GPIOB
#define MINI_COM3_TX_GPIO_CLK            RCM_APB2_PERIPH_GPIOB
#define MINI_COM3_RX_PIN                 GPIO_PIN_11
#define MINI_COM3_RX_GPIO_PORT           GPIOB
#define MINI_COM3_RX_GPIO_CLK            RCM_APB2_PERIPH_GPIOB
#define MINI_COM3_IRQn                   USART3_IRQn


/*
********************************************************************************
*                              	局部变量
********************************************************************************
*/
USART_T* COM_USART[COMn] = {MINI_COM1, MINI_COM2, MINI_COM3};

GPIO_T* COM_TX_PORT[COMn] = {MINI_COM1_TX_GPIO_PORT, MINI_COM2_TX_GPIO_PORT, MINI_COM3_TX_GPIO_PORT};

GPIO_T* COM_RX_PORT[COMn] = {MINI_COM1_RX_GPIO_PORT, MINI_COM2_RX_GPIO_PORT, MINI_COM3_RX_GPIO_PORT};

const uint32_t COM_USART_CLK[COMn] = {MINI_COM1_CLK, MINI_COM2_CLK, MINI_COM3_CLK};

const uint32_t COM_TX_PORT_CLK[COMn] = {MINI_COM1_TX_GPIO_CLK, MINI_COM2_TX_GPIO_CLK, MINI_COM3_TX_GPIO_CLK};

const uint32_t COM_RX_PORT_CLK[COMn] = {MINI_COM1_RX_GPIO_CLK, MINI_COM2_RX_GPIO_CLK, MINI_COM3_RX_GPIO_CLK};

const uint16_t COM_TX_PIN[COMn] = {MINI_COM1_TX_PIN, MINI_COM2_TX_PIN, MINI_COM3_TX_PIN};

const uint16_t COM_RX_PIN[COMn] = {MINI_COM1_RX_PIN, MINI_COM2_RX_PIN, MINI_COM3_RX_PIN};

/*
********************************************************************************
*                               全局变量
********************************************************************************
*/

/*
********************************************************************************
*                             	函数声明
********************************************************************************
*/




/*
********************************************************************************
*                              	函数定义
********************************************************************************
*/
static void UartVarInit (COM_TypeDef COM, USART_Config_T* configStruct)
{
	 GPIO_Config_T GPIO_configStruct;

    /* Enable GPIO clock */
    RCM_EnableAPB2PeriphClock(COM_TX_PORT_CLK[COM] | COM_RX_PORT_CLK[COM]|RCM_APB2_PERIPH_AFIO);
	
    if (COM == COM1)
    {
        RCM_EnableAPB2PeriphClock(COM_USART_CLK[COM]);
    }
    else
    {
        RCM_EnableAPB1PeriphClock(COM_USART_CLK[COM]);
    }

    /* Configure USART Tx as alternate function push-pull */
    GPIO_configStruct.mode = GPIO_MODE_AF_PP;
    GPIO_configStruct.pin = COM_TX_PIN[COM];
    GPIO_configStruct.speed = GPIO_SPEED_50MHz;
    GPIO_Config(COM_TX_PORT[COM], &GPIO_configStruct);

    /* Configure USART Rx as input floating */
    GPIO_configStruct.mode = GPIO_MODE_IN_FLOATING;
    GPIO_configStruct.pin = COM_RX_PIN[COM];
    GPIO_Config(COM_RX_PORT[COM], &GPIO_configStruct);

    /* USART configuration */
    USART_Config(COM_USART[COM], configStruct);

    /* Enable USART */
    USART_Enable(COM_USART[COM]);
}

int fputc(int ch, FILE* f)
{
    /* send a byte of data to the serial port */
    USART_TxData(DEBUG_USART, (uint8_t)ch);

    /* wait for the data to be send  */
    while (USART_ReadStatusFlag(DEBUG_USART, USART_FLAG_TXBE) == RESET);

    return (ch);
}

/*!
 * @brief 初始化串口硬件
 */
void Drv_InitUart(void)
{
	 USART_Config_T USART_ConfigStruct;


    RCM_EnableAPB2PeriphClock((RCM_APB2_PERIPH_T)(RCM_APB2_PERIPH_GPIOA | RCM_APB2_PERIPH_USART1));
    RCM_EnableAPB1PeriphClock(RCM_APB1_PERIPH_USART2);
	
	
    USART_ConfigStruct.baudRate = 115200;
    USART_ConfigStruct.hardwareFlow = USART_HARDWARE_FLOW_NONE;
    USART_ConfigStruct.mode = USART_MODE_TX_RX;
    USART_ConfigStruct.parity = USART_PARITY_NONE;
    USART_ConfigStruct.stopBits = USART_STOP_BIT_1;
    USART_ConfigStruct.wordLength = USART_WORD_LEN_8B;
	
	UartVarInit(COM1, &USART_ConfigStruct);
	GPIO_ConfigPinRemap(GPIO_REMAP_USART2);
    UartVarInit(COM2, &USART_ConfigStruct);
	
	UartVarInit(COM3, &USART_ConfigStruct);
//	UartVarInit();		/* 必须先初始化全局变量,再配置硬件 */

//	InitHardUart();		/* 配置串口的硬件参数(波特率等) */

//	ConfigUartNVIC();	/* 配置串口中断 */
}

uint8_t txBuf[] = "Hello USART1 \r\n \r\n";
uint8_t tx1Buf[] = "Hello USART2 \r\n \r\n";
uint8_t tx3Buf[] = "Hello USART3 \r\n \r\n";
//uint8_t tx3Buf[] = "0X11 0X12 0X13 0X14 0X15 0X16 \r\n";
/*底层串口测试*/
uint16_t count=0;
void Drv_Uart_TEST(void){
	uint8_t i;
	
	
	if(++count%3==0){
		printf("我是串口3--USART3 \r\n \r\n");
	for(i = 0; i < sizeof(txBuf); i++)
	{
		while(USART_ReadStatusFlag(USART1, USART_FLAG_TXBE) == RESET);
		USART_TxData(USART1, txBuf[i]);
		
		while(USART_ReadStatusFlag(USART2, USART_FLAG_TXBE) == RESET);
		USART_TxData(USART2, tx1Buf[i]);
		
		while(USART_ReadStatusFlag(USART3, USART_FLAG_TXBE) == RESET);
		USART_TxData(USART3, tx3Buf[i]);
		
	}
}
}


/*** (C) COPYRIGHT 2023 Co-Win Hydrogen Power (Guangdong) Co., Ltd (文件结束) ***/
