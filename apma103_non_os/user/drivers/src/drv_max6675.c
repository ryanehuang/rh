/*******************************************************************************
 *                            APPLICATION CODE
 *
 *(C) Copyright 2023; Co-Win Hydrogen Power (Guangdong) Co., Ltd
 *
 * @file    drv_max6675.c
 * @brief   热电偶温度读取
 * @author  XieXunjie
 * @version 0.0.1
 * @date    2023-01-13
 *
 * -----------------------------------------------------------------------------
 *
 * -----------------------------------------------------------------------------
 * 文件修改历史：
 * <时间>           | <版本>    | <作者>        | <描述>
 * <2023-01-13>     | v0.0.1    | XieXunjie     | 模板文件
 * -----------------------------------------------------------------------------
 ******************************************************************************/

/*
********************************************************************************
*                              头文件包含
********************************************************************************
*/
#include "drv_max6675.h"
#include "drv_spi_bus.h"
#include "user_config.h"
/*
********************************************************************************
*                              	本地宏指令定义
********************************************************************************
*/
/*温度数据*/
Thermocouple_typeDef 	t_Therm;
#if 0

#define ANALOG_TEMP_TO_MAX6675_LSB_RATIO   		4.0f 


#define MAX6675_CS_MAX					    2u

#define MAX6675_SO_PORT  					GET_PIN(B, 14)
#define MAX6675_SCK_PORT  					GET_PIN(B, 13)

#define MAX6675_CS_PORT_0					GET_PIN(B, 15)	/*0:	*/
#define MAX6675_CS_PORT_1					GET_PIN(D, 8)	/*1:	*/
#define MAX6675_CS_PORT_2					0           	/*2:	*/
#define MAX6675_CS_PORT_3					0           	/*3:	*/
#define MAX6675_CS_PORT_4					0				/*4:	*/
#define MAX6675_CS_PORT_5					0				/*5:	*/
#define MAX6675_CS_PORT_6					0				/*6:	*/
#define MAX6675_CS_PORT_7					0				/*7:	*/
#define MAX6675_CS_PORT_8					0				/*8:	*/
#define MAX6675_CS_PORT_9					0				/*9:	*/
#define MAX6675_CS_PORT_10					0				/*10:	*/
#define MAX6675_CS_PORT_11					0				/*11:	*/
#define MAX6675_CS_PORT_12					0				/*12:	*/
#define MAX6675_CS_PORT_13					0				/*13:	*/
#define MAX6675_CS_PORT_14					0				/*14:	*/
#define MAX6675_CS_PORT_15					0				/*15:	*/

#define __MAX6675_PORT(x)  				MAX6675_CS_PORT_##x
#define MAX6675_PORT(x)  				x==0?__MAX6675_PORT(0): \
											x==1?__MAX6675_PORT(1): \
											x==2?__MAX6675_PORT(2): \
											x==3?__MAX6675_PORT(3): \
											x==4?__MAX6675_PORT(4): \
											x==5?__MAX6675_PORT(5): \
											x==6?__MAX6675_PORT(6): \
											x==7?__MAX6675_PORT(7): \
											x==8?__MAX6675_PORT(8): \
											x==9?__MAX6675_PORT(9): \
											x==10?__MAX6675_PORT(10): \
											x==11?__MAX6675_PORT(11): \
											x==12?__MAX6675_PORT(12): \
											x==13?__MAX6675_PORT(13): \
											x==14?__MAX6675_PORT(14): \
											x==15?__MAX6675_PORT(15): \
											__MAX6675_PORT(15)
														
/*设置SPI--MAX6675模式*/
#define SetMAX6675Mode(x) 					pin_mode(MAX6675_PORT(x), PIN_MODE_Out_PP);

#define SetMAX6675Out(x,y) 					y==PIN_LOW?pin_write(MAX6675_PORT(x), PIN_LOW):pin_write(MAX6675_PORT(x), PIN_HIGH)

//#define SetMAX6675SOMode()					pin_mode(MAX6675_SO_PORT,PIN_MODE_AF_PP)
//#define SetMAX6675SCKMode()					pin_mode(MAX6675_SCK_PORT,PIN_MODE_AF_PP)



//读MAX6675数据时，读取时钟需要保持的系统时钟周期数
#define MAX6675_DATA_CLK_DELAY_SYSTEM_CYCLES    (SystemCoreClock/1000000 + 1)    		
//读MAX6675数据时，读取时钟就绪后，为保证准确而将读取动作延后的系统时钟数
#define MAX6675_DATA_READ_DELAY_SYSTEM_CYCLES   ((MAX6675_DATA_CLK_DELAY_SYSTEM_CYCLES + 1) / 2)


#define SPI2_SCK_PORT  						GET_PIN(B, 13)
#define SPI2_MISO_PORT  					GET_PIN(B, 14)
#define SPI2_MOSI_PORT  					0

/*SPI时钟*/
#define SetSPI2_SCKMode()     	   			pin_mode(SPI2_SCK_PORT,PIN_MODE_AF_PP)
/*SPI 主机输入，从机输出*/
#define SetSPI2_MISOMode()    	       		pin_mode(SPI2_MISO_PORT,PIN_MODE_AF_PP)
/*SPI 主机输出，从机输入*/
#define SetSPI2_MOSIMode()    	       		pin_mode(SPI2_MOSI_PORT,PIN_MODE_AF_PP)
#endif


#define MAX6675_SCK_PORT			GPIOE
#define MAX6675_SCK_PIN 			GPIO_PIN_10

#define MAX6675_SO_PORT				GPIOE
#define MAX6675_SO_PIN 				GPIO_PIN_12

#define MAX6675_CS_PORT				GPIOE
#define MAX6675_CS_PIN 				GPIO_PIN_15

#define MAX6675_CS2_PORT			GPIOB
#define MAX6675_CS2_PIN 			GPIO_PIN_1



#define ANALOG_TEMP_TO_MAX6675_LSB_RATIO   		4 


//读MAX6675数据时，读取时钟需要保持的系统时钟周期数
#define MAX6675_DATA_CLK_DELAY_SYSTEM_CYCLES    (SystemCoreClock/1000000 + 1)    		
//读MAX6675数据时，读取时钟就绪后，为保证准确而将读取动作延后的系统时钟数
#define MAX6675_DATA_READ_DELAY_SYSTEM_CYCLES   ((MAX6675_DATA_CLK_DELAY_SYSTEM_CYCLES + 1) / 2)

#define SPI_MAX6675_CS_1_HIGH          GPIO_SetBit(MAX6675_CS_PORT,MAX6675_CS_PIN)
#define SPI_MAX6675_CS_1_LOW           GPIO_ResetBit(MAX6675_CS_PORT,MAX6675_CS_PIN)

#define SPI_MAX6675_CS_2_HIGH          GPIO_SetBit(MAX6675_CS2_PORT,MAX6675_CS2_PIN)
#define SPI_MAX6675_CS_2_LOW           GPIO_ResetBit(MAX6675_CS2_PORT,MAX6675_CS2_PIN)

/*
********************************************************************************
*                              	局部变量
********************************************************************************
*/


/*
********************************************************************************
*                               全局变量
********************************************************************************
*/

/*
********************************************************************************
*                             	函数声明
********************************************************************************
*/

/**
 * @brief :	Max6675 使能通道
 * @param :	none.
 * @retval:	none.
 */
static void Drv_MAX6675_Channel_Slect(uint8_t i_u8Channel)
{
    switch(i_u8Channel){
        case 1:
			SPI_MAX6675_CS_1_LOW;
            break;
        case 2:
            SPI_MAX6675_CS_2_LOW;
            break;
        default:
            break;
    }
}

/**
 * @brief :	Max6675 失能通道
 * @param :	none.
 * @retval:	none.
 */
static void Drv_MAX6675_Channel_Not_Slect(uint8_t i_u8Channel)
{
    switch(i_u8Channel){
        case 1:
            SPI_MAX6675_CS_1_HIGH;
            break;
        case 2:
            SPI_MAX6675_CS_2_HIGH;
            break;
        default:
            break;
    }
}


/*!
 * @brief 读取热电偶MAX6675初始化
 *
 * 备注：
 */
void Drv_Max6675Init(void)
{	
	/*******************************
	* 	此处有个迷：
	* 1.SPI在片选之后直接初始化（不使用封装函数），正常使用
	* 2.SPI初始化封装成函数在片选之后，无法使用
	* 3.SPI初始化封装成函数在片选之前，正常使用
	*******************************/
	/*SPI1初始化*/
	Drv_SPI1_Init();
	/*CS片选失能*/
	Drv_MAX6675_Channel_Not_Slect(1);
	
	
    /*CS初始化*/
//	for (uint8_t i=0;i<MAX6675_CS_MAX;i++){
//		/*初始化CS*/
//		SetMAX6675Mode(i);
//		/*CS片选失能*/
//		SetMAX6675Out(i,PIN_HIGH);
//	}
	
} 



/*
********************************************************************************
*                              	函数定义
********************************************************************************
*/

/*!
 * @brief 读取热电偶数值
 *
 * 备注：K型热电偶量程-200~+1350℃
 * @return  真实温度数值，精度0.1	错误返回-200℃
 */
float Drv_MAX6675_Temp_Read(uint8_t i_u8Channel, uint8_t *err)
{
    uint16_t u16ChannelTemp = 0;
	uint32_t i = 0;
	//MAX6675通道片选选中
	Drv_MAX6675_Channel_Slect(i_u8Channel);	
	
	for(i = 0; i < MAX6675_DATA_READ_DELAY_SYSTEM_CYCLES; i++){};

	u16ChannelTemp|=SPI_ReceiveByte()<<8;	
	u16ChannelTemp|=SPI_ReceiveByte()<<0;
//	/*读取热电偶数据*/
//	u16ChannelTemp|=SPI_ReadWriteByte(SPI1,0xFF)<<8;  
//	u16ChannelTemp|=SPI_ReadWriteByte(SPI1,0xFF)<<0;
//	printf("SPI_ReadWriteByte(SPI1,0xFF):%0x\r\n",SPI_ReadWriteByte(SPI1,0xFF));
	//MAX6675通道片选不选
    Drv_MAX6675_Channel_Not_Slect(i_u8Channel);
		
	for(i = 0; i < MAX6675_DATA_READ_DELAY_SYSTEM_CYCLES; i++){};
    if((u16ChannelTemp & (1 << 15 | 4)) == 0) { 										//数据正确且热电偶均未断线
        u16ChannelTemp &= 0x7FFF;
        u16ChannelTemp >>= 3;
        *(err + i_u8Channel) = 0;
		return u16ChannelTemp/ANALOG_TEMP_TO_MAX6675_LSB_RATIO;		
    } else {
        u16ChannelTemp = 0xF9C;  //3996
        *(err + i_u8Channel) = 1;
		return -200.0f;
    }

}
static void Updata_MAX6675(void)
{
	static uint8_t timeCount = 0;
	if(++timeCount %1 == 0){//200
		timeCount = 0;
		uint8_t err[2];
		t_Therm.Therm_In[0] = (uint16_t)((Drv_MAX6675_Temp_Read(0,err)+200)*10);
		t_Therm.Therm_In[1] = (uint16_t)((Drv_MAX6675_Temp_Read(1,err)+200)*10);
	}


}
/*!
 * @brief 测试热电偶MAX6675
 *
 * 备注：
 */
void Drv_MAX6675_Test(void)
{
	Updata_MAX6675();
	
//	printf("MAX6675_ReadTemperature:%f\r\n",MAX6675_ReadTemperature());
	
	printf("temp1:%f\r\n",t_Therm.Therm_In[0]*0.1-200);
	printf("temp2:%f\r\n",t_Therm.Therm_In[1]*0.1-200);
//	printf("temp1:%d\r\n",(uint16_t)((Drv_MAX6675_Temp_Read(1,err)+200)*10));
//	printf("spi1:%d\r\n",err[1]);
}


/*** (C) COPYRIGHT 2023 Co-Win Hydrogen Power (Guangdong) Co., Ltd (文件结束) ***/
