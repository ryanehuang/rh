/*******************************************************************************
 *                            APPLICATION CODE
 *
 *(C) Copyright 2023; Co-Win Hydrogen Power (Guangdong) Co., Ltd
 *
 * @file    drv_can.c
 * @brief   CAN底层驱动模块
 * @author  XieXunjie
 * @version 0.0.2
 * @date    2023-03-02
 *
 * -----------------------------------------------------------------------------
 *
 * -----------------------------------------------------------------------------
 * 文件修改历史：
 * <时间>           | <版本>    | <作者>        | <描述>
 * <2023-03-02>     | v0.0.2    | XieXunjie     | CAN底层驱动模块
 * <2023-01-13>     | v0.0.1    | XieXunjie     | 模板文件
 * -----------------------------------------------------------------------------
 ******************************************************************************/

/*
********************************************************************************
*                              包含头文件
********************************************************************************
*/
#include "drv_can.h"
#include "user_config.h"


/*
********************************************************************************
*                              	本地宏指令定义
********************************************************************************
*/


/*STM32F103C8T6风扇板*/
#if (Board_Choose==0)
//CAN接收RX0中断使能
#define CAN_RX0_INT_ENABLE	1		//0,不使能;1,使能.	
/*STM32F103C8T6 IO拓展板*/
#elif (Board_Choose==1)
//CAN接收RX0中断使能
#define CAN_RX0_INT_ENABLE	1		//0,不使能;1,使能.	
#elif (Board_Choose==2)
/*STM32F103VCT6 风扇板*/
//CAN接收RX0中断使能
#define CAN_RX0_INT_ENABLE	1		//0,不使能;1,使能.	
#elif (Board_Choose==3)
/*STM32F103VCT6 动环板*/
//CAN接收RX0中断使能
#define CAN_RX0_INT_ENABLE	1		//0,不使能;1,使能.	
#elif (Board_Choose==4)
/*APM32--动环板*/
/*CAN1*/
/*
PD0-RX PD1-TX//重定义功能
*/

#define		CAN1_CLK						RCM_APB2_PERIPH_GPIOD
#define		CAN1_RX_PORT					GPIOD
#define		CAN1_RX_PIN						GPIO_PIN_0

#define		CAN1_TX_PORT					GPIOD
#define		CAN1_TX_PIN						GPIO_PIN_1


/*CAN2*/
/*
PB12-RX PB13-TX//默认功能
*/
#define		CAN2_CLK						RCM_APB2_PERIPH_GPIOB
#define		CAN2_RX_PORT					GPIOB
#define		CAN2_RX_PIN						GPIO_PIN_12

#define		CAN2_TX_PORT					GPIOB
#define		CAN2_TX_PIN						GPIO_PIN_13


//CAN接收RX0中断使能
#define CAN_RX0_INT_ENABLE	1		//0,不使能;1,使能.	
#else

#define		CAN_RX_1					GET_PIN(A, 11)	/*CAN1 RX*/
#define		CAN_TX_1					GET_PIN(A, 12)	/*CAN1 TX*/
#define 	CAN_Remap_INT_ENABLE	0	//复用引脚：1,不使能;1,使能.
//CAN接收RX0中断使能
#define CAN_RX0_INT_ENABLE	1		//0,不使能;1,使能.	
#endif


/*
********************************************************************************
*                              	局部变量
********************************************************************************
*/


/*
********************************************************************************
*                               全局变量
********************************************************************************
*/
/*can1接收链路输入回调函数*/
//CPU_FNCT_CAN 	CAN1Confg_LinkRe_byte_InCB;

/*
********************************************************************************
*                             	函数声明
********************************************************************************
*/



/*
********************************************************************************
*                              	函数定义
********************************************************************************
*/



/*!
 * @brief 	CAN1 GPIO初始化 
 *	PD0-RX PD1-TX//重定义功能
 * @return  无
 */
static void CAN1_GPIO_Config(void)
{
	//重定义功能
	RCM_EnableAPB2PeriphClock(RCM_APB2_PERIPH_AFIO);
    RCM_EnableAPB2PeriphClock(CAN1_CLK);

    GPIO_Config_T configStruct;
	/*开启CAN1复用*/
    GPIO_ConfigPinRemap(GPIO_REMAP2_CAN1);
	
    configStruct.pin   = CAN1_TX_PIN;       // CAN1 Tx remap 2
    configStruct.mode  = GPIO_MODE_AF_PP;
    configStruct.speed = GPIO_SPEED_50MHz;
    GPIO_Config(CAN1_TX_PORT, &configStruct);

    configStruct.pin = CAN1_RX_PIN;         // CAN1 Rx remap 2
    configStruct.mode = GPIO_MODE_IN_PU;
    GPIO_Config(CAN1_RX_PORT, &configStruct);
	
}


/*!
 * @brief 	CAN2 GPIO初始化
 * @return  无
 */
static void CAN2_GPIO_Config(void)
{
	RCM_EnableAPB2PeriphClock(RCM_APB2_PERIPH_AFIO);
    RCM_EnableAPB2PeriphClock(CAN2_CLK);

    GPIO_Config_T configStruct;

    configStruct.pin   = CAN2_TX_PIN;       // CAN1 Tx remap 2
    configStruct.mode  = GPIO_MODE_AF_PP;
    configStruct.speed = GPIO_SPEED_50MHz;
    GPIO_Config(CAN2_TX_PORT, &configStruct);

    configStruct.pin = CAN2_RX_PIN;         // CAN1 Rx remap 2
    configStruct.mode = GPIO_MODE_IN_PU;
    GPIO_Config(CAN2_RX_PORT, &configStruct);
}

/*!
 * @brief 	CAN1配置初始化
 * @return  
 */
static void CAN1_Init(void)
{
	RCM_EnableAPB1PeriphClock(RCM_APB1_PERIPH_CAN1);
    CAN_Config_T       canConfig;
    CAN_FilterConfig_T FilterStruct;
	
    CAN_Reset(CAN1);

    CAN_ConfigStructInit(&canConfig);

    /* CAN1 and CAN2  init */
    canConfig.autoBusOffManage = DISABLE;
    canConfig.autoWakeUpMode   = DISABLE;
    canConfig.nonAutoRetran    = DISABLE;
    canConfig.rxFIFOLockMode   = DISABLE;
    canConfig.txFIFOPriority   = ENABLE;
    canConfig.mode             = CAN_MODE_NORMAL;
    
	/*
	CAN初始化
	tsjw:重新同步跳跃时间单元.范围:CAN_SJW_1tq~ CAN_SJW_4tq
	tbs2:时间段2的时间单元.   范围:CAN_BS2_1tq~CAN_BS2_8tq;
	tbs1:时间段1的时间单元.   范围:CAN_BS1_1tq ~CAN_BS1_16tq
	brp :波特率分频器.范围:1~1024;  tq=(brp)*tpclk1
	波特率=Fpclk1/((tbs1+1+tbs2+1+1)*brp);
	mode:CAN_Mode_Normal,普通模式;CAN_Mode_LoopBack,回环模式;
	Fpclk1的时钟在初始化的时候设置为36M,如果设置CAN_Mode_Init(CAN_SJW_1tq,CAN_BS2_8tq,CAN_BS1_9tq,4,CAN_Mode_LoopBack);
	则波特率为:36M/((8+9+1)*4)=500Kbps
	返回值:0,初始化OK;
		其他,初始化失败;
		
		初始化can:	
		CAN_Mode_Init(CAN_SJW_1,CAN_TIME_SEGMENT2_7,CAN_TIME_SEGMENT1_10,4,CAN_MODE_NORMAL);//CAN初始化环回模式,波特率500Kbps   
	*/
	
	/*500k*/
	canConfig.syncJumpWidth    = CAN_SJW_1;
	canConfig.timeSegment1     = CAN_TIME_SEGMENT1_10;
    canConfig.timeSegment2     = CAN_TIME_SEGMENT2_7;
    canConfig.prescaler = 4;
	

    CAN_Config(CAN1, &canConfig);

    FilterStruct.filterNumber = 1;
    FilterStruct.filterMode = CAN_FILTER_MODE_IDMASK;
    FilterStruct.filterScale = CAN_FILTER_SCALE_32BIT;
    FilterStruct.filterIdHigh = 0x6420;
    FilterStruct.filterIdLow  = 0x0000;
    FilterStruct.filterMaskIdHigh = 0x0000;
    FilterStruct.filterMaskIdLow  = 0x0000;
    FilterStruct.filterFIFO = CAN_FILTER_FIFO_0;
    FilterStruct.filterActivation =  ENABLE;

    CAN_ConfigFilter(CAN1, &FilterStruct);

}

/*!
 * @brief 	CAN2配置初始化
 * @return  
 */
static void CAN2_Init(void)
{
    RCM_EnableAPB1PeriphClock(RCM_APB1_PERIPH_CAN2);

    CAN_Config_T       canConfig;
    CAN_FilterConfig_T FilterStruct;
	
    CAN_Reset(CAN2);

    CAN_ConfigStructInit(&canConfig);

    /* CAN1 and CAN2  init */
    canConfig.autoBusOffManage = DISABLE;
    canConfig.autoWakeUpMode   = DISABLE;
    canConfig.nonAutoRetran    = DISABLE;
    canConfig.rxFIFOLockMode   = DISABLE;
    canConfig.txFIFOPriority   = ENABLE;
    canConfig.mode             = CAN_MODE_NORMAL;
	
   /*
	CAN初始化
	tsjw:重新同步跳跃时间单元.范围:CAN_SJW_1tq~ CAN_SJW_4tq
	tbs2:时间段2的时间单元.   范围:CAN_BS2_1tq~CAN_BS2_8tq;
	tbs1:时间段1的时间单元.   范围:CAN_BS1_1tq ~CAN_BS1_16tq
	brp :波特率分频器.范围:1~1024;  tq=(brp)*tpclk1
	波特率=Fpclk1/((tbs1+1+tbs2+1+1)*brp);
	mode:CAN_Mode_Normal,普通模式;CAN_Mode_LoopBack,回环模式;
	Fpclk1的时钟在初始化的时候设置为36M,如果设置CAN_Mode_Init(CAN_SJW_1tq,CAN_BS2_8tq,CAN_BS1_9tq,4,CAN_Mode_LoopBack);
	则波特率为:36M/((8+9+1)*4)=500Kbps
	返回值:0,初始化OK;
		其他,初始化失败;
		
		初始化can:	
		CAN_Mode_Init(CAN_SJW_1,CAN_TIME_SEGMENT2_7,CAN_TIME_SEGMENT1_10,4,CAN_MODE_NORMAL);//CAN初始化环回模式,波特率500Kbps   
	*/

	/*500k*/
	canConfig.syncJumpWidth    = CAN_SJW_1;
    canConfig.timeSegment1     = CAN_TIME_SEGMENT1_10;
    canConfig.timeSegment2     = CAN_TIME_SEGMENT2_7;
    canConfig.prescaler = 4;

    CAN_Config(CAN2, &canConfig);

    FilterStruct.filterNumber = 1;
    FilterStruct.filterMode = CAN_FILTER_MODE_IDMASK;
    FilterStruct.filterScale = CAN_FILTER_SCALE_32BIT;
    FilterStruct.filterIdHigh = 0x6420;
    FilterStruct.filterIdLow  = 0x0000;
    FilterStruct.filterMaskIdHigh = 0x0000;
    FilterStruct.filterMaskIdLow  = 0x0000;
    FilterStruct.filterFIFO = CAN_FILTER_FIFO_0;
    FilterStruct.filterActivation =  ENABLE;

    CAN_ConfigFilter(CAN2, &FilterStruct);
}


/*!
 * @brief 	CAN底层初始化
 * @return  
 */
void Drv_InitCANx(void)
{
	/*CAN1 GPIO初始化*/
	CAN1_GPIO_Config();
	/*CAN1 GPIO初始化*/
	CAN2_GPIO_Config();
	
	/*CAN1配置初始化 */
	CAN1_Init();
	/*CAN2配置初始化 */
	CAN2_Init();
	
}



 	    
/*!
    \brief      can 0 接收数据
    \param[in]  none
    \param[out] none
    \retval     none
*/

//__weak void Can0_ConfgLinkRebyteIn(uint32_t StdId,uint32_t ExtId,uint8_t *Can_Data,uint8_t Len,uint8_t ide)   
//{

//}




/*!
 * @brief			can发送一组数据(固定格式:ID为0X12,标准帧,数据帧)
 * @param[in] msg	消息数组
 * @param[in] len	消息长度
 * @return			0,成功;
					其他,失败;
 */
uint8_t Can_Send_Msg(uint8_t* msg,uint8_t len)
{	
	uint8_t mbox;
	uint16_t i=0;
	CAN_TxMessage_T   TxMessage;
	TxMessage.stdID=0x12;			// 标准标识符 
	TxMessage.extID=0x12;			// 设置扩展标示符 
	TxMessage.typeID=CAN_TYPEID_STD; 	// 标准帧
	TxMessage.remoteTxReq=CAN_RTXR_DATA;		// 数据帧
	TxMessage.dataLengthCode=len;				// 要发送的数据长度
	for(i=0;i<len;i++)
	TxMessage.data[i]=msg[i];			          
	mbox= CAN_TxMessage(CAN2, &TxMessage);   
	i=0; 
	while((CAN_TxMessageStatus(CAN2, mbox)==DEF_FALSE)&&(i<0XFFF))i++;	//等待发送结束
	if(i>=0XFFF)return 1;
	return 0;	 
}


/*!
 * @brief :	can发送一帧数据(固定格式:标准ID + 扩展ID + 标准帧 + 数据帧)
 * @param :	CANx:CAN接口,CAN1或CAN2;len:数据长度(最大为8);msg:数据指针,最大为8个字节;ID:标示符.
 * @retval:	0,成功;其他,失败;
 */
static uint8_t CANx_Send_Msg(CAN_T* CANx, Message* msg)
{	
	uint8_t mbox;
	uint16_t i=0;
	CAN_TxMessage_T   TxMessage;
	TxMessage.stdID=(uint32_t)msg->cob_id;	 	// 标准标识符为0
	TxMessage.extID=msg->exd_id;	 			// 设置扩展标示符 
	TxMessage.typeID=msg->ide;     				// 使用标准标识符
	TxMessage.remoteTxReq=msg->rtr;		 	 			// 消息类型为数据帧，一帧8位
	TxMessage.dataLengthCode=msg->len;						// 发送两帧信息
	for(i=0;i<msg->len;i++)
		TxMessage.data[i]=msg->data[i];			// 第一帧信息          
	mbox= CAN_TxMessage(CANx, &TxMessage);    
	i=0;
	while((CAN_TxMessageStatus(CANx, mbox)==DEF_FALSE)&&(i<0XFFF))i++;	//等待发送结束
	if(i>=0XFFF)return 1;
	return 0;	
}



/**
 * @brief :	can发送节点信息
 * @param :	CANx:CAN接口,CAN1或CAN2;i_Msglen:数据长度;msg:数据指针;i_NodeId：id,ExtId：拓展id.
 * @retval:	0,成功;其他,失败;
 */
static uint8_t SendCanMsgContainNodeId(CAN_T * CANx,uint32_t i_Msglen, uint8_t *msg, uint32_t i_NodeId,uint32_t ExtId,uint8_t Ide)
{
    uint8_t ret;
    uint32_t i, j;
    uint8_t  u8SendErrCount = 0;
    uint8_t  LastTimeSendErrFlag = DEF_NO;
    Message ProcessedData;   

    ProcessedData.cob_id = i_NodeId ;//CAN ID
	ProcessedData.exd_id = ExtId;
	ProcessedData.rtr = CAN_RTXR_DATA;
	/*帧格式*/
	ProcessedData.ide = Ide;
    for(i = 0; i < (i_Msglen / 8 + ((i_Msglen % 8) ? 1 : 0)); i++) { //长消息拆包的分包数,8为CAN消息的标准包长度
        if(LastTimeSendErrFlag == DEF_NO) {
            ProcessedData.len = ((i_Msglen - i * 8) > 8) ? 8 : (i_Msglen - i * 8);//每个短包的数据长度，除最后一包需要另算外，其余均为8

            for(j = 0; j < ProcessedData.len; j++) {
                ProcessedData.data[j] = msg[i * 8 + j];
            }        
        } else {
            //上次发送出错，直接发送数组即可，不需要重新复制
        }

        ret = CANx_Send_Msg(CANx, &ProcessedData);

        if(ret != 0) {
            LastTimeSendErrFlag = DEF_YES;
            i--;

            if(++u8SendErrCount >= 10) {
                break;
            }
        } else {
            LastTimeSendErrFlag = DEF_NO;//发送成功，清零该标志，进入下一次循环，发送该数据
        }   
    }
    return ret;
}

/**
* @brief :	can发送一帧数据
 * @param :	CANx:CAN接口,CAN1或CAN2;
 * @retval:	
 */
void CAN_Send_Data(CAN_T * CANx,uint32_t StdId,uint32_t ExtId,uint8_t *pbuffer,uint8_t Len,uint8_t ide)
{
	SendCanMsgContainNodeId(CANx,Len,pbuffer,StdId,ExtId,ide);
}


/**
 * @brief :	can接收一帧数据
 * @param :	CANx:CAN接口,CAN1或CAN2;i_u8RxBuf:数据指针
 * @retval:	0,成功;其他,失败;
 */
//uint8_t Can_Receive_Msg(uint8_t *buf)
//{		   		   
//// 	uint32_t i;
////	CanRxMsg RxMessage;
////    if( CAN_MessagePending(CAN1,CAN_FIFO0)==0)return 0;		//没有接收到数据,直接退出 
////    CAN_Receive(CAN1, CAN_FIFO0, &RxMessage);//读取数据	
////    for(i=0;i<8;i++)
////    buf[i]=RxMessage.Data[i];  
////	return RxMessage.DLC;	
//}

///**
//* @brief :	设置can1接收链路输入回调函数
// * @param :	
// * @retval:	
// */
//void  Bsp_SetCAN1Confg_LinkRe_byte_InCB(CPU_FNCT_CAN p)
//{
//	CAN1Confg_LinkRe_byte_InCB = p;
//}

/**
* @brief :	can测试
 */
static uint8_t cnt=0;
void Drv_can_test(void)
{
	static uint8_t i=0;
	static uint8_t canbuf[8];
	 

	for(i=0;i<8;i++)
		{
			canbuf[i]=cnt+i;//填充发送缓冲区
			printf("canbuf[%d]:%d",i,canbuf[i]);
		}
		//cnt+=8;
		cnt++;
	if(i%80==0)
	{
		cnt=0;
	}
		printf("===canbuf[%d]:%d",i,canbuf[i]);
		CAN_Send_Data(CAN2,Board_ID,Board_ID,canbuf,8,CAN_TYPEID_EXT);	
//		CAN_Send_Data(CAN1,Board_ID,Board_ID,canbuf,8,CAN_TYPEID_EXT);	
//		CAN_Send_Data(CAN1,0Xbb,0Xbb,canbuf,8,CAN_TYPEID_STD);	
//		CAN_Send_Data(CAN1,0Xcc,0Xcc,canbuf,8,CAN_TYPEID_STD);
//		CAN_Send_Data(CAN1,0Xdd,0Xdd,canbuf,8,CAN_TYPEID_EXT);
		CAN_Send_Data(CAN2,0X02,0X02,canbuf,8,CAN_TYPEID_STD);	
		CAN_Send_Data(CAN2,0Xa2,0Xa2,canbuf,8,CAN_TYPEID_STD);
	
//		Can_Send_Msg(canbuf, 8);//发送8个字节 
}



/***************************** (文件结束) *********************************/
